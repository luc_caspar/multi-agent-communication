#!/usr/bin/env python3
from loguru import logger
from pettingzoo.classic import go_v5
from agents import GoAgent, HumanAgent, RandomAgent
from common import Cli, MainLoop
from settings import DATA_DIR


def test(mode, epochs, board_size, komi, config_files, max_iter, human, random, headless, video, init_epoch=0):
    """
    Initialize both environment and players in test mode and run the corresponding main loop.
    :param mode: An instance of the utils.Mode enumeration describing the mode in which the game is to be executed.
    :param epochs: The number of epochs the main loop should run for.
    :param board_size: The size of the Go board on which the agents will be trained.
    Standard sizes are: 9, 13, and 19 (full board).
    :param komi: A float representing the handicap awarded to the white player for always being the second to
    make a move.
    :param config_files: A list of Paths to files containing the weights and biases for the different agents.
    :param max_iter: The maximum number of steps a single game is allowed to run for.
    :param human: A boolean flag indicating if one of the players should be a human agent.
    :param random: A boolean flag indicating if one of the players should be a random agent.
    :param headless: A boolean flag describing the type of environment the program is run in.
    :param video: A boolean flag enabling the generation of separate video files for each epoch of the main loop.
    :param init_epoch: TODO
    :return: Nothing.
    """

    if human and random:
        raise RuntimeError('Error: Cannot instantiate both random and human agents at the same time.')

    # Initialize the environment
    env = go_v5.env(board_size=board_size, komi=komi, render_mode='rgb_array')

    if human or random:
        # Initialize the rest of the players
        players = {}
        for name in env.possible_agents[1:]:
            obs_space = env.observation_space(name)['observation'].shape
            players[name] = GoAgent(name, obs_space)

        if config_files is not None:
            for player, file in zip(players.values(), config_files):
                logger.info(f'Using configuration located at: {file}')
                player.load_config(file)

        # Initialize the human or random agent
        rh_name = env.possible_agents[0]
        action_space = env.action_space(rh_name)
        players[rh_name] = HumanAgent(action_space) if human else RandomAgent(rh_name, action_space)
    else:
        players = {}
        for name in env.possible_agents:
            obs_space = env.observation_space(name)['observation'].shape
            players[name] = GoAgent(name, obs_space)

        if config_files is not None:
            for player, file in zip(players.values(), config_files):
                logger.info(f'Using configuration located at: {file}')
                player.load_config(file)

    # Create Main
    main_loop = MainLoop(env, players, headless, video)

    # Run Main
    metrics = main_loop.run(mode, epochs, max_iter, init_epoch=init_epoch)

    # Record the metrics to disk for later processing
    data_dir = DATA_DIR.joinpath(f'Go_{board_size}')
    if not data_dir.exists():
        data_dir.mkdir(mode=0o775)
    data_file = data_dir.joinpath(f'test_{board_size}.h5')
    metrics.to_hdf(data_file, mode='a', key='root', complevel=9, format='table', index=False)


def train(lr_rate, mode, epochs, board_size, komi, config_files, max_iter, self_play, epoch_thres, init_epoch=0):
    """
    Initialize both environment and players in train mode and run the corresponding main loop.
    :param lr_rate: A float describing how fast the agent's parameters should update for each update step.
    :param mode: An instance of the utils.Mode enumeration describing the mode in which the game is to be executed.
    :param epochs: The number of epochs the main loop should run for.
    :param board_size: The size of the Go board on which the agents will be trained.
    Standard sizes are: 9, 13, and 19 (full board).
    :param komi: A float representing the handicap awarded to the white player for always being the second to
    make a move.
    :param config_files: A list of Paths to files containing the weights and biases for the different agents.
    :param max_iter: The maximum number of steps a single game is allowed to run for.
    :param self_play: A flag indicating whether the agent is playing against itself or another learning agent.
    :param epoch_thres: The number of epochs to wait between two checkpointing events.
    :param init_epoch: TODO
    :return: Nothing.
    """

    # Initialize the environment
    env = go_v5.env(board_size=board_size, komi=komi)

    # Initiate go playing agents
    if self_play:
        obs_space = env.observation_space(env.possible_agents[0])['observation'].shape
        if config_files is not None:
            logger.info(f'Using configuration located at: {config_files[0]}')
            agent = GoAgent('self_play', obs_space, lr_rate, config_file=config_files[0])
        else:
            agent = GoAgent('self_play', obs_space, lr_rate)
        players = {name: agent for name in env.possible_agents}
    else:
        players = {}
        for name in env.possible_agents:
            obs_space = env.observation_space(name)['observation'].shape
            players[name] = GoAgent(name, obs_space, lr_rate)

        if config_files is not None:
            for player, file in zip(players.values(), config_files):
                logger.info(f'Using configuration located at: {file}')
                player.load_config(file)

    # Create the main loop
    main_loop = MainLoop(env, players)

    # Run Main
    metrics = main_loop.run(mode, epochs, max_iter, epoch_save_thres=epoch_thres, init_epoch=init_epoch)

    # Record the metrics to disk for later processing
    data_dir = DATA_DIR.joinpath(f'Go_{board_size}')
    if not data_dir.exists():
        data_dir.mkdir(mode=0o775)
    data_file = data_dir.joinpath(f'train_{board_size}.h5')
    metrics.to_hdf(data_file, mode='a', key='root', complevel=9, format='table', index=False)


if __name__ == "__main__":
    # Define a command line interface
    cli = Cli(train_func=train, test_func=test,
              desc='Allows to train Go playing agents, save/load their internal states, and test the results of their'
                   ' training.')

    cli.add_argument('-s', '--size', dest='board_size', type=int,
                     help='The size of the Go board on which the agents will be trained/tested.\nTypical values are: '
                          '9, 13 or 19 (full game).', default=9)
    cli.add_argument('-k', '--komi', dest='komi', type=float, help='The amount of handicap given to white for starting '
                                                                   'second.', default=7.5)
    cli.add_argument('--init-epoch', dest='init_epoch', type=int,
                     help='Which epoch to start the training/testing process at.', default=0)

    # Parse the command line arguments
    parsed_args = cli.parse()

    # Convert the number of epochs into int
    try:
        parsed_args.epochs = int(parsed_args.epochs)
    except ValueError:
        exit(-1)

    # Extract the command to execute
    cmd = parsed_args.func
    del parsed_args.func

    # Execute the requested sub-command
    cmd(**vars(parsed_args))
