#!/usr/bin/env python3
import numpy as np
from random import choice
from subprocess import Popen, PIPE, DEVNULL, TimeoutExpired
from lc0.backends import Weights, Backend, GameState
from settings import KATAGO_EXEC, KATAGO_CFG, LC0_DIR
from loguru import logger


class KataGoController(object):
    """
    Define a controller to send command to the underlying KataGo process and parse the results of GTP commands.
    In essence, this class allows one to talk to KataGo from any python script.
    """

    def __init__(self, model_path, cfg_path=KATAGO_CFG, exec_path=KATAGO_EXEC):
        """
        Start the child KataGo process using the given configuration
        :param model_path: A pathlib.Path instance representing the absolute path to the model to be loaded and used in
        by KataGo.
        :param cfg_path: A pathlib.Path instance representing the absolute path to the configuration file to be used by
        KataGo.
        :param exec_path: A pathlib.Path instance representing the absolute path to KataGo's executable.
        """

        super(KataGoController).__init__()

        # Declare parameters related to KataGo's engine and board
        self._board_size = 19  # Default in KataGo

        # Build the command line to execute KataGo
        cmd = [str(exec_path), 'gtp', '-model', str(model_path), '-config', str(cfg_path)]

        # Start KataGo in a child process
        self.proc = Popen(cmd, stdin=PIPE, stdout=PIPE, stderr=DEVNULL, text=True, encoding='utf-8')

    def __str__(self):
        self.send_cmd('name')
        success, name = self.read_out()
        if not success:
            return 'unknown unknown'

        self.send_cmd('version')
        success, version = self.read_out()
        if not success:
            return f'{name} unknown'

        return f'{name} {version}'

    def send_cmd(self, cmd, *args):
        """
        Send commands to KataGo using the GTP protocol.
        :param cmd: A string representing the command to send.
        :param args: Any additional string arguments that should be given along with the command.
        :return nothing.
        """

        # Make sure the engine is still active
        exit_code = self.proc.poll()
        if exit_code is None:
            # Add any arguments to the end of the command separating them by a blank space
            if args:
                cmd = ' '.join([cmd.strip()] + list(map(lambda s: s.strip(), args)))
            # Send the command
            self.proc.stdin.write(f'{cmd}\n')
            self.proc.stdin.flush()
        else:
            logger.exception(f"An error occurred while trying to send a command:\nThe process seem to have exited "
                             f"with code {exit_code}")
            raise RuntimeError(f"An error occurred while trying to send a command:\nThe process seem to have exited "
                               f"with code {exit_code}")

    def read_out(self, no_status=False):
        """
        Read the result of a command from the standard output.
        :param no_status: A boolean flag indicating whether to consider the first character as the command's status
        or not. This is useful for multipart outputs (e.g.: `kata-raw-nn all` has 8 outputs for a single command and
        a single status).
        :return: A tuple, whose first component is a boolean flag marking the successful execution of the command.
        The second part is the response (or error message) returned by KataGo, if any.
        If the no_status flag is set to True, the method only returns KataGo's response.
        """

        # Make sure the engine is still active
        exit_code = self.proc.poll()
        if exit_code is None:
            # Read the engine's output from the standard output
            c = self.proc.stdout.read(1)
            data = []
            while c != '\n' or data[-1] != '\n':
                data.append(c)
                if self.proc.poll() is not None:
                    break
                c = self.proc.stdout.read(1)

            if no_status:
                # Compile the data and return as is
                return ''.join(data).strip()
            else:
                # Compile the characters into a complete message
                status, *message = data
                message = ''.join(message).strip()
                # Return status and message
                # status == '=' is a success
                # status == '?' is a failure
                return status == '=', message
        else:
            logger.exception(f"An error occurred while trying to read the result of a command:"
                             f"\nThe process seem to have exited with code {exit_code}")
            raise RuntimeError(f"An error occurred while trying to read the result of a command:"
                               f"\nThe process seem to have exited with code {exit_code}")

    def close(self):
        """
        Tries to stop the child process as cleanly as possible.
        :return: Nothing.
        """

        self.send_cmd('quit')
        self.read_out()
        try:
            # The timeout parameter is in seconds
            exit_code = self.proc.wait(timeout=5)
            logger.info(f"KataGo child process exited with code {exit_code}")
        except TimeoutExpired:
            self.proc.terminate()
            try:
                exit_code = self.proc.wait(timeout=5)
                logger.error(f"KataGo child process exited with code {exit_code}")
            except TimeoutExpired:
                self.proc.kill()
                logger.exception(f"KataGo child process exited with code {exit_code}")

    def help(self):
        """
        List all available commands for this specific controller.
        :return: A list of strings representing the commands available for the KataGo engine.
        """
        self.send_cmd('list_commands')
        success, commands = self.read_out()
        if not success:
            return []
        else:
            return commands.splitlines()

    def showboard(self):
        """
        Print the current state of the board, as well as additional parameters in use by the KataGo engine.
        :return: A string representing the current board's state and KataGo's parameters.
        """

        self.send_cmd('showboard')
        return self.read_out()[1]

    def set_boardsize(self, size):
        """
        Set the size of the board with which KataGo is playing.
        :param size: An integer representing the length or width of the board.
        :return: A boolean flag indicating whether the operation succeeded or not.
        """
        # Set the internal parameter
        self._board_size = size
        # Send the command, converting the size to str
        self.send_cmd('boardsize', str(size))
        # Read and return the status
        return self.read_out()[0]

    def set_komi(self, komi):
        """
        Set the komi with which KataGo is playing.
        :param komi: A float representing the handicap given to White for starting to move on the second turn.
        :return: A boolean flag indicating whether the operation succeeded or not.
        """
        # Send the command, converting the komi to str
        self.send_cmd('komi', str(komi))
        # Read and return the status
        return self.read_out()[0]

    def reset(self):
        """
        Clear the board, reset the moves history, and the number of captured stones.
        :return: A boolean flag indicating whether the operation succeeded or not.
        """
        self.send_cmd('clear_board')
        return self.read_out()[0]

    def play(self, name, vertex):
        """
        Let katago know that a move has been played. KataGo should add the move to the history, hence updating its
        internal board state.
        :param name: A string representing the name of the player: W(hite) or B(lack).
        :param vertex: A string encoding the player's move. The special vertex 'pass' is also available.
        :return: A boolean flag indicating whether the operation succeeded or not.
        """

        self.send_cmd('play', name, vertex)
        return self.read_out()[0]

    def genmove(self, name):
        """
        Request KataGo to generate a move for the given player, based on the move history and current state of the
        board.
        :param name: A string representing the player's name: W(hite) or B(lack).
        :return: A boolean flag indicating whether the operation succeeded or not, and an encoded string representing
        the next move to perform.
        """

        self.send_cmd('genmove', name)
        return self.read_out()

    def get_policy_slow(self):
        """
        Get the probabilities associated with each action.
        This is the rather slow version, which averages the probabilities over all 8 symmetries.
        :return: A tuple containing: a boolean flag indicating whether the operation was a success, and an array
        containing the probabilities for each move.
        """

        # Request KataGo to compute the probabilities associated with all moves
        self.send_cmd('kata-raw-nn', 'all')

        # Read the first part of the message
        messages = []
        status, msg = self.read_out()

        # Skip the rest of the response if the operation was unsuccessful
        if not status:
            return False, []

        # Process the rest of the response
        messages.append(msg)
        messages += [self.read_out(no_status=True) for _ in range(7)]

        # Process each message to extract the corresponding policy
        policies = []
        for msg in messages:
            policies.append(self.elems2policy(msg.split(), nan=0))
        policies = np.array(policies)

        return True, np.nanmean(policies, axis=0).tolist()

    def get_policy_fast(self):
        """
        Get the probabilities associated with each action.
        This is the rather faster version, which randomly sample a policy from the 8 available symmetries.
        :return: A tuple containing: a boolean flag indicating whether the operation was a success, and an array
        containing the probabilities for each move.
        """

        # Request KataGo to compute the probabilities associated with all moves
        self.send_cmd('kata-raw-nn', str(choice(range(8))))

        # Read the first part of the message
        status, msg = self.read_out()

        # Skip the rest of the response if the operation was unsuccessful
        if not status:
            return False, []

        # Process the message to extract the corresponding policy
        return True, self.elems2policy(msg.split(), nan=0)

    def elems2policy(self, elems, nan=np.nan):
        """
        Extract a policy from a list of elements.
        :param elems: A list of string elements encoding the policy.
        :param nan: An object to use a replacement for 'NAN' strings contained in the given list of elements.
        :return: A list of probabilities corresponding to the policy.
        """

        policy = elems[21:21 + self._board_size ** 2] + [elems[21 + self._board_size ** 2 + 1]]
        return list(map(lambda e: nan if e == 'NAN' else float(e), policy))


class Lc0Controller(object):
    """
    Define a controller for interacting with a pre-trained instance of Leela Chess Zero.
    More details about the architecture and implementation can be found at:
    https://github.com/LeelaChessZero/lc0
    """

    def __init__(self, nn_size='m'):
        """
        Define the properties needed to communicate with an instance of Leela Chess Zero.
        :param nn_size: A character indicating the size of the network, and therefore the weights, to use for the
        underlying Leela Chess Zero.
        """

        # Initialize the parent class
        super(Lc0Controller).__init__()

        # Instantiate the weights, backend and move history
        match nn_size:
            case "s":
                w = Weights(str(LC0_DIR.joinpath('195b450999e874d07aea2c09fd0db5eff9d4441ec1ad5a60a140fe8ea94c4f3a')))
            case "m":
                w = Weights(str(LC0_DIR.joinpath('7ca2381cfeac5c280f304e7027ffbea1b7d87474672e5d6fb16d5cd881640e04')))
            case "l":
                w = Weights(str(LC0_DIR.joinpath('600469c425eaf7397138f5f9edc18f26dfaf9791f365f71ebc52a419ed24e9f2')))
            case _:
                raise NotImplementedError("Only 'l', 'm' and 's' are valid network size.")
        self._network = Backend(weights=w)
        self._mv_hist = []

    def get_policy(self):
        """
        Based on the current state of the board, compute the probability associated with each legal move.
        :return: Three lists.
        The first list contains the legal moves, the second one contains the corresponding indices,
        while the last includes the corresponding probabilities.
        """

        # Define the current state of the game
        g_state = GameState(moves=self._mv_hist)

        # Activate Leela Chess Zero
        i = g_state.as_input(self._network)
        out, = self._network.evaluate(i)

        # Return a list of moves and their associated probabilities
        return g_state.moves(), g_state.policy_indices(), out.p_softmax(*g_state.policy_indices())

    def get_score_info(self):
        """
        Based on the current state of the board, compute the expected Win, Draw, and Loss rates.
        :return: A tuple containing the different information in order: W, D, L.
        """

        # Define the current state of the game
        g_state = GameState(moves=self._mv_hist)

        # Activate Leela Zero Chess
        out, = self._network.evaluate(g_state.as_input(self._network))

        # Compute and return the relevant score related information
        draw = out.d()
        exp_score = out.q()
        win = (1 + exp_score - draw) / 2
        loss = (1 - (exp_score + draw)) / 2
        return win, draw, loss

    def play(self, move):
        """
        Indicate to the agent which move has just been played.
        :param move: An index corresponding to
        """

        # Append the move to the history of all moves
        self._mv_hist.append(move)

    def reset(self):
        """
        Clean the move history to start a new game.
        """
        self._mv_hist = []

    @property
    def last_move(self):
        """
        Return the last move played.
        """
        try:
            return self._mv_hist[-1]
        except IndexError:
            return None


if __name__ == '__main__':
    from operator import itemgetter

    # Have a lc0 play against itself
    lc0 = Lc0Controller()

    while True:
        policy = lc0.get_policy()
        if len(policy) == 0:
            break

        mv, prob = max(policy, key=itemgetter(1))
        print(f'Playing move: {mv}, with proba: {prob}')
        lc0.play(mv)
