#!/usr/bin/env python3
from sys import exit
from argparse import ArgumentParser
from pathlib import Path
import pandas as pd
from matplotlib import pyplot as plt
import h5py
from pettingzoo.classic import go_v5
from settings import DATA_DIR

# Instantiate a Go environment to extract required information
ENV = go_v5.env()

# Define a command line allowing the user to provide some of the script arguments
parser = ArgumentParser()
parser.add_argument('--config_save_thres',
                    dest='config_save_thres',
                    type=int,
                    help='The number of epochs between two configuration checkpoints',
                    default=1000)
parser.add_argument('-f', '--data_path',
                    dest='data_path',
                    type=str,
                    help='The relative path to the datafile from which to extract and plot relevant metrics.', required=True)
ex_grp = parser.add_mutually_exclusive_group(required=True)
ex_grp.add_argument('-b', '--black',
                    dest='player_name',
                    action='store_const', const=ENV.possible_agents[0],
                    help='A flag indicating that the agent was playing using the black stones during the benchmark.')
ex_grp.add_argument('-w', '--white',
                    dest='player_name',
                    action='store_const', const=ENV.possible_agents[1],
                    help='A flag indicating that the agent was playing using the white stones during the benchmark.')


args = parser.parse_args()

# Define required parameters
PLAYER_NAME = args.player_name
EPOCH_SAVE_THRES = args.config_save_thres
ENV_NAME = ENV.metadata['name']
DATA_PATH = Path(args.data_path).expanduser().resolve()
if not DATA_PATH.exists():
    exit(-1)

# Get a list of keys contained in the data file
with h5py.File(DATA_PATH, 'r') as f:
    keys = list(f.keys())

# Extract the list of epochs from the keys
epochs = [k.split('_')[-1] for k in keys]

# Replace the 'final' epoch with the correct value
try:
    # This and the .insert() statement below are called in an effort to maintain ordering,
    # and mapping between epochs and keys
    final_idx = epochs.index('final')
    epochs.pop(final_idx)
    epochs = list(map(int, epochs))
    epoch_max = max(epochs) + EPOCH_SAVE_THRES
    epochs.insert(final_idx, epoch_max)
except ValueError:
    epochs = list(map(int, epochs))
    epoch_max = max(epochs)

scores = []
avg_mvs = []
max_mvs = []
min_mvs = []
for key, epoch in zip(keys, epochs):
    df = pd.read_hdf(DATA_PATH, key=key)
    # Extract the scores for all epochs
    score = df[f'score_{PLAYER_NAME}'].value_counts()
    score.name = epoch
    scores.append(score)
    # Extract the number of moves played for all epochs
    avg_mvs.append(df[f'moves_{PLAYER_NAME}'].mean())
    max_mvs.append(df[f'moves_{PLAYER_NAME}'].max())
    min_mvs.append(df[f'moves_{PLAYER_NAME}'].min())

player_data = pd.concat(scores, axis=1).T
player_data = player_data.sort_index(axis=0)

moves = pd.Series(data=avg_mvs, index=epochs)
min_mvs = pd.Series(data=min_mvs, index=epochs)
max_mvs = pd.Series(data=max_mvs, index=epochs)
moves.sort_index(axis=0)
min_mvs.sort_index(axis=0)
max_mvs.sort_index(axis=0)
min_max = [moves - min_mvs, max_mvs - moves]

# Create the figure and appropriate sub-plot
fig, axs = plt.subplots(nrows=2, sharex=True)
fig.suptitle('Benchmark against KataGo')

# Plot the data corresponding to the scores
player_data[['W', 'I', 'L']].rename(columns={'W': 'Win', 'I': 'Illegal Move', 'L': 'Loss'}).plot(use_index=True,
                                                                                                 ax=axs[0])

# Configure the axis
axs[0].set_ylabel('Percentage')
axs[0].set_xlabel('Training epochs')
axs[0].grid(True, which='both', axis='both')
axs[0].minorticks_on()
axs[0].set_xbound(lower=0, upper=epoch_max)
axs[0].set_ybound(lower=0)

# Plot the data corresponding to the number of moves
moves.plot(use_index=True, ax=axs[1], yerr=min_max)
axs[1].set_ylabel('Moves')
axs[1].set_xlabel('Training epochs')
axs[1].grid(True, which='both', axis='both')
axs[1].set_ybound(lower=0)

# Save the plot to file and display it on screen
fig.savefig(DATA_DIR.joinpath(f'{DATA_PATH.stem}.png'), format='png')
plt.show()
