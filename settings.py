#!/usr/bin/env python3
from sys import platform
from pathlib import Path
import torch


# Define the repository as the root
ROOT_DIR = Path(__file__).parent

# Define the device PyTorch will be using for computation
PT_DEVICE = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

# Define folders for storing the videos, configurations, and data
CONF_DIR = ROOT_DIR.joinpath('Configs')
if not CONF_DIR.exists():
    CONF_DIR.mkdir(mode=0o775)

VIDEO_DIR = ROOT_DIR.joinpath('Videos')
if not VIDEO_DIR.exists():
    VIDEO_DIR.mkdir(mode=0o775)

DATA_DIR = ROOT_DIR.joinpath('Data')
if not DATA_DIR.exists():
    DATA_DIR.mkdir(mode=0o775)

# Define the directory, the name of the executable, and the default configuration file for KataGo
KATAGO_DIR = ROOT_DIR.joinpath('..', 'KataGo').resolve()

if platform in ['win32', 'cygwin']:
    KATAGO_EXEC = KATAGO_DIR.joinpath('katago.exe')
elif platform == 'linux':
    KATAGO_EXEC = KATAGO_DIR.joinpath('katago')
else:
    KATAGO_EXEC = ''

KATAGO_CFG = KATAGO_DIR.joinpath('default_gtp.cfg')

# Define constants required for running Leela Chess Zero
LC0_DIR = ROOT_DIR.joinpath('lc0')
