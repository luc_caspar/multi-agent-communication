#!/usr/bin/env python3
import pandas as pd
from matplotlib import pyplot as plt

with open('go_v5.log', 'r', encoding='utf8') as f:
    lines = f.readlines()

data = [''.join(line.split('{')[1:]).strip(' \n') for line in lines if line.find('avg_loss_') != -1]
data = [d.replace('None', 'NaN') for d in data]
data = [d.split(', ') for d in data]

epochs = [int(d[0].split(': ')[-1]) for d in data]

nb_moves_black = pd.Series([d[2].split(': ')[-1] for d in data], index=epochs).astype('int', copy=False)
nb_moves_white = pd.Series([d[6].split(': ')[-1] for d in data], index=epochs).astype('int', copy=False)
df_moves = pd.DataFrame({'White': nb_moves_white, 'Black': nb_moves_black}, index=epochs)

avg_loss_b = [d[3].split(': ')[-1] for d in data]
avg_loss_w = [d[7].split(': ')[-1] for d in data]
s_loss_b = pd.Series(avg_loss_b).astype('float', copy=False)
s_loss_w = pd.Series(avg_loss_w).astype('float', copy=False)
df_loss = pd.DataFrame({'White': s_loss_w, 'Black': s_loss_b}, index=epochs)

fig, axs = plt.subplots(nrows=2, sharex=True)

df_loss.plot(ax=axs[0])
axs[0].set_xlabel('Epochs')
axs[0].set_ylabel('Average loss')

df_moves.plot(ax=axs[1])
axs[1].set_ylabel('Number of moves')
axs[1].set_ybound(lower=0)
for ax in axs:
    ax.set_xbound(lower=0)
    ax.minorticks_on()
    ax.grid(visible=True, which='both', axis='both')

plt.show()
