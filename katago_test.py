#!/usr/bin/env python3
from pettingzoo.classic import go_v5
from common import MainLoop
from agents import KataGoAgent
from utils import Mode

BOARD_SIZE = 9
KOMI = 7.5

env = go_v5.env(board_size=BOARD_SIZE, komi=KOMI, render_mode='rgb_array')
players = {}
for name in env.possible_agents:
    players[name] = KataGoAgent(name, BOARD_SIZE, KOMI)

try:
    main_loop = MainLoop(env, players, headless=False, video=False)
    main_loop.run(Mode.TEST, 1, max_iter=1e9)
finally:
    for player in players.values():
        player.close()
