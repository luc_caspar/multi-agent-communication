#!/usr/bin/env python3
from agents import C4A0Agent, C4A1Agent, C4A2Agent, C4B0Agent, C4B1Agent, C4B2Agent, HumanAgent, RandomAgent
from pettingzoo.classic import connect_four_v3
from common import Cli, MainLoop
from settings import DATA_DIR


def get_agent_from_str(arch):
    if arch == 'A0':
        return C4A0Agent
    elif arch == 'A1':
        return C4A1Agent
    elif arch == 'A2':
        return C4A2Agent
    elif arch == 'B0':
        return C4B0Agent
    elif arch == 'B1':
        return C4B1Agent
    elif arch == 'B2':
        return C4B2Agent
    else:
        raise ValueError('Error: The agent type required does not exist.')


def test(arch, mode, epochs, config_files, max_iter, human, random, headless, video):
    """
    Initialize both environment and players in test mode and run the corresponding main loop.
    :param arch: A string representing the architecture (and therefore type of agent) to instantiate.
    :param mode: An instance of the utils.Mode enumeration describing the mode in which the game is to be executed.
    :param epochs: The number of epochs the main loop should run for.
    :param config_files: A list of Paths to files containing the weights and biases for the different agents.
    :param max_iter: The maximum number of steps a single game is allowed to run for.
    :param human: A boolean flag indicating if one of the players should be a human agent.
    :param random: A boolean flag indicating if one of the players should be a random agent.
    :param headless: A boolean flag describing the type of environment the program is run in.
    :param video: A boolean flag enabling the generation of separate video files for each epoch of the main loop.
    :return: Nothing.
    """

    if human and random:
        raise RuntimeError('Error: Cannot instantiate both random and human agents at the same time.')

    # Initialize the environment and its associated data directory
    env = connect_four_v3.env(render_mode='rgb_array')

    # Initialize the players
    agt_class = get_agent_from_str(arch)

    data_dir = DATA_DIR.joinpath(arch)
    if not data_dir.exists():
        data_dir.mkdir(mode=0o775)

    if human or random:
        # Initialize the players
        players = {}
        for name in env.possible_agents[1:]:
            players[name] = agt_class(name)

        if config_files is not None:
            for player, file in zip(players.values(), config_files):
                player.load_config(file)

        # Initialize the human or random agent
        rh_name = env.possible_agents[0]
        action_space = env.action_space(rh_name)
        players[rh_name] = HumanAgent(action_space) if human else RandomAgent(rh_name, action_space)
    else:
        # Initialize the players
        players = {}
        for name in env.possible_agents:
            players[name] = agt_class(name)

        if config_files is not None:
            for player, file in zip(players.values(), config_files):
                player.load_config(file)

    # Create the main loop
    main_loop = MainLoop(env, players, headless=headless, video=video)

    # Run the main loop
    metrics = main_loop.run(mode, epochs, max_iter)

    # Save the dataframe containing the different metrics to disk
    file_name = '_'.join([p.name for p in players.values()])
    data_path = str(data_dir.joinpath(f"""testing_{file_name}.h5"""))
    metrics.to_hdf(data_path, key='root', mode='w', complevel=9, format='table', index=False)


def train(arch, lr_rate, mode, epochs, config_files, max_iter, self_play, epoch_thres):
    """
    Initialize both environment and players in train mode and run the corresponding main loop.
    :param arch: A string representing the architecture (and therefore type of agent) to instantiate.
    :param lr_rate: A float describing how fast the agent's parameters should update for each update step.
    :param mode: An instance of the utils.Mode enumeration describing the mode in which the game is to be executed.
    :param epochs: The number of epochs the main loop should run for.
    :param config_files: A list of Paths to files containing the weights and biases for the different agents.
    :param max_iter: The maximum number of steps a single game is allowed to run for.
    :param self_play: A flag indicating whether the agent is playing against itself or another learning agent.
    :param epoch_thres: The number of epochs to wait between two checkpointing events.
    :return: Nothing.
    """

    # Initialize the environment
    env = connect_four_v3.env()

    # Initialize the players and its associated data directory
    agt_class = get_agent_from_str(arch)

    data_dir = DATA_DIR.joinpath(arch)
    if not data_dir.exists():
        data_dir.mkdir(mode=0o775)

    if self_play:
        if config_files is not None:
            agent = agt_class('self_play', lr_rate, config_file=config_files[0])
        else:
            agent = agt_class('self_play', lr_rate)
        players = {name: agent for name in env.possible_agents}
    else:
        players = {}
        for name in env.possible_agents:
            players[name] = agt_class(name, lr_rate)

        if config_files is not None:
            for player, file in zip(players.values(), config_files):
                player.load_config(file)

    # Create the main loop
    main_loop = MainLoop(env, players)

    # Run the main loop
    metrics = main_loop.run(mode, epochs, max_iter, epoch_thres)

    # Save the dataframe containing the different metrics to disk
    file_name = '_'.join([p.name for p in players.values()])
    data_path = str(data_dir.joinpath(f"""training_{file_name}.h5"""))
    metrics.to_hdf(data_path, key='root', mode='w', complevel=9, format='table', index=False)


if __name__ == "__main__":
    # Define a command line interface
    cli = Cli(train_func=train,
              test_func=test,
              desc='Allows to train agents to play connect four, save/load their internal states, and test the results'
                   'of their training.')

    # Add flags for the type of architecture
    ex_grps = cli.add_mutually_exclusive_group(required=True)
    for ex_grp in ex_grps:
        ex_grp.add_argument('-A0', dest='arch', action='store_const', const='A0', help='Initialize the an agent with '
                                                                                       'the A0 architecture.')
        ex_grp.add_argument('-A1', dest='arch', action='store_const', const='A1', help='Initialize the an agent with '
                                                                                       'the A1 architecture.')
        ex_grp.add_argument('-A2', dest='arch', action='store_const', const='A2', help='Initialize the an agent with '
                                                                                       'the A2 architecture.')
        ex_grp.add_argument('-B0', dest='arch', action='store_const', const='B0', help='Initialize the an agent with '
                                                                                       'the B0 architecture.')
        ex_grp.add_argument('-B1', dest='arch', action='store_const', const='B1', help='Initialize the an agent with '
                                                                                       'the B1 architecture.')
        ex_grp.add_argument('-B2', dest='arch', action='store_const', const='B2', help='Initialize the an agent with '
                                                                                       'the B2 architecture.')

    # Parse command line arguments
    parsed_args = cli.parse()

    # Convert the number of epochs into int
    try:
        parsed_args.epochs = int(parsed_args.epochs)
    except ValueError:
        exit(-1)

    # Extract the command to execute
    cmd = parsed_args.func
    del parsed_args.func

    # Execute the requested sub-command
    cmd(**vars(parsed_args))
