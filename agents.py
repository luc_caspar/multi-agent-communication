#!/usr/bin/env python3
"""
A module containing the definition of all agents available for this project.
"""
from abc import ABC, abstractmethod, ABCMeta
import math
from random import random, choice
from string import ascii_uppercase
import torch.nn.utils
import torch.optim as optim
from torchinfo import summary
import numpy as np
from scipy.spatial import distance
from loguru import logger
from networks import *
from controllers import KataGoController, Lc0Controller
from utils import *
from settings import *


class Agent(ABC):
    """
    Define an interface for all agents.
    """

    def __init__(self, name):
        """
        Initialize properties common to all agents.
        :param name: A string identifying a specific instance of an agent.
        """
        self.name = name
        self.nb_moves = 0

    @abstractmethod
    def update(self, *args, **kwargs):
        """
        Given the current state of both agent and environment, as well as the
        reward received for the previous action update the agent's internal
        state.
        """

    @abstractmethod
    def get_action(self, *args, **kwargs):
        """
        Given the current state of both the agent and the environment, return
        the next action to perform.
        """

    @abstractmethod
    def save_config(self, file_path):
        """
        Save the agent's internal state to file.
        :param file_path: The path to the file where the configuration should be saved.
        """

    @abstractmethod
    def load_config(self, file_path):
        """
        Load the agent's internal state from the given file.
        :param file_path: The path to the file from where the configuration should be loaded.
        """


class DQNAgent(Agent, metaclass=ABCMeta):
    """
    Define an abstract agent which updates its neural network using Q-learning. The brain of the agent is made of a
    Deep Neural Network (most likely a CNN). It stores a map between state space and expected value.
    """

    def __init__(self, name, policy_net, target_net, optimizer, loss, gamma, batch_size, tgt_update_thres,
                 mem_size=100000, config_file=None):
        """
        Define a generic DQN-based agent, which uses both policy and target networks. Its weights and biases are
        optimized following the Q-learning algorithm and using an Adam optimizer.
        :param name: A string identifying a specific instance of an agent.
        :param policy_net: A torch.nn.Module which represent a specific neural architecture. In this case the network
        is used to approximate the agent's policy.
        :param target_net: A torch.nn.Module which represent a target network. The target is a checkpoint of the policy
        network and provides stable expected values for training.
        :param optimizer: A torch.nn.Optimizer to be used during the training process to update the policy network's
        parameters.
        :param loss: A torch.nn instance representing the loss function to use for updating the agent's parameters.
        :param gamma: A float representing how future reward are weighted relative to short-term ones.
        :param batch_size: An integer defining how many data point to sample for training the agent on each loop.
        :param tgt_update_thres: An integer representing the number of learning steps to wait before updating the
        target network's weights.
        :param mem_size: An integer used as the upper bond on the size of the memory for the agent.
        :param config_file: A pathlib.Path to the file containing values to be loaded in the policy and target
        networks as initial parameters.
        """

        # Initialize the parent class
        super(DQNAgent, self).__init__(name)

        # Initialize the agent's main properties
        self.step_cnt = 0
        self.memory = ReplayMemory(max_size=mem_size)
        self.optimizer = optimizer
        self.loss = loss
        self.q_net = policy_net
        self.tgt_net = target_net

        # Initialize other useful properties
        self.gamma = gamma
        self.batch_size = batch_size
        self.tgt_update_thres = tgt_update_thres

        # Load the weights and biases from the configuration file if any is provided
        self.config_file = config_file
        if config_file is not None:
            self.q_net.load_state_dict(torch.load(config_file, map_location=PT_DEVICE))

        # Make sure the target network is the same as the policy network
        self.tgt_net.load_state_dict(self.q_net.state_dict())
        self.tgt_net.eval()

    def __str__(self):
        """
        Describe a specific instance of an agent.
        :return: A string.
        """
        return f'Agent {self.name}\n' \
               f'\tParameters loaded from: {self.config_file}\n' \
               f'\tNetwork Architecture:\n{summary(self.q_net, verbose=0, device=PT_DEVICE)}'

    def update(self, state, action, next_state, reward, done):
        """
        Adapt the policy network's parameters based on: the previous state, next state, action performed to transition
        between them, and reward received.
        :param state: A tensor describing the previous state the agent was in.
        :param action: A tensor corresponding to the action taken to move from the previous to the next state.
        :param next_state: A tensor describing the next state the agent ended up in.
        :param reward: A tensor corresponding to the reward received for performing the associated action in the
        previous state.
        :param done: A flag indicating whether the game is done or not.
        :return: A float representing the loss.
        """
        # Put the policy network back into training mode to avoid any issues
        self.q_net.train()

        # Insert the transition in memory
        self.memory.push(state, action, next_state, reward, done)

        # If possible sample a new batch of memories to learn from
        if len(self.memory) < self.batch_size:
            return None
        transitions = self.memory.sample(self.batch_size)

        # Convert the batch of transitions into a transition of batches
        batch = Transitions(*zip(*transitions))

        # Then extract the different batches
        state_batch = torch.stack(batch.state)
        action_batch = torch.stack(batch.action)
        next_state_batch = torch.stack(batch.next_state)
        reward_batch = torch.stack(batch.reward)
        done_batch = batch.done

        # Compute the values of the state-action pairs
        state_action_values = self.q_net(state_batch)
        # Detach and clone the state values which will be used to compute the expected state values
        expected_state_values = state_action_values.detach().clone()

        # Compute the values of the next states
        # Max returns both the max values (in position 0) and the indices at which those values where
        # found (in position 1)
        with torch.no_grad():
            max_state_values = self.tgt_net(next_state_batch).max(1)[0].unsqueeze(1).detach()

        # Compute the expected Q values
        for i in range(state_action_values.size()[0]):
            if done_batch[i]:
                expected_state_values[i, action_batch[i].item()] = reward_batch[i]
            else:
                expected_state_values[i, action_batch[i].item()] = max_state_values[i] * self.gamma + reward_batch[i]

        # Compute the loss
        loss = self.loss(state_action_values, expected_state_values)
        loss_value = loss.item()

        # Optimize the policy network
        self.optimizer.zero_grad()
        loss.backward()
        self.optimizer.step()

        # Update the step counter
        self.step_cnt += 1

        # If necessary update the target network
        if self.step_cnt % self.tgt_update_thres == 0:
            self.tgt_net.load_state_dict(self.q_net.state_dict())
            self.tgt_net.eval()

        # Return the loss for monitoring purposes
        return loss_value

    def save_config(self, game, epoch=None):
        """
        Save the agent's internal weights and biases to file.
        :param game: A string identifying the game being played.
        :param epoch: An integer identifying from which training epoch the configuration comes from.
        :return: Nothing.
        """

        # Define the file name based on the game, epoch, and player's name
        if epoch is None:
            file_path = CONF_DIR.joinpath(f'{game}_{self.name}_final').with_suffix('.pth')
        else:
            file_path = CONF_DIR.joinpath(f'{game}_{self.name}_{epoch}').with_suffix('.pth')

        # Use torches built-in utilities for saving network states
        torch.save(self.q_net.state_dict(), file_path)

    def load_config(self, file_path):
        """
        Load the Q-network's weights and biases from the given file path.
        :param file_path: A string representing the file from which to load the parameters.
        :return: Nothing.
        """

        # Record the path
        self.config_file = file_path

        # Load the Q-network parameters
        self.q_net.load_state_dict(torch.load(file_path, map_location=PT_DEVICE))

        # Copy the parameters to the target network as well
        self.tgt_net.load_state_dict(torch.load(file_path, map_location=PT_DEVICE))
        self.tgt_net.eval()


class GoAgent(DQNAgent):
    """
    Defines an agent specifically for playing the game of Go as described in:
    https://pettingzoo.farama.org/environments/classic/go/
    This specific agent uses a Softmax policy to decide on the next action to perform.
    """

    def __init__(self, name, obs_shape, l_rate=1e-2, gamma=0.99, batch_size=64, tgt_update_thres=10,
                 mem_size=500000, config_file=None):
        """
        Initialize the Policy and Target network to fit the constraints of the specific Go environment.
        :param name: A string identifying a specific instance of an agent.
        :param obs_shape: A gymnasium.space.Space representing the space of possible observations returned by the
        environment when playing the specific game of Go.
        :param l_rate: A float representing the rate at which network parameters are adapted during learning.
        :param gamma: A float representing how future reward are weighted relative to short-term ones.
        :param batch_size: An integer defining how many data point to sample for training the agent on each loop.
        :param tgt_update_thres: An integer representing the number of learning steps to wait before updating the
        target network's weights.
        :param mem_size: An integer used as the upper bond on the size of the memory for the agent.
        :param config_file: A pathlib.Path to the file containing the initial values to be used for the policy
        network's parameters.
        """
        # Extract the parameters required to instantiate the policy and target networks
        height, width, channel = obs_shape

        # Compute the number of available actions from the board size
        outputs = (height * width) + 1

        # Instantiate a new optimizer and loss for the policy network
        policy_net = DQNGo(height, width, channel, outputs)
        optimizer = optim.SGD(policy_net.parameters(), lr=l_rate, weight_decay=1e-4, momentum=0.9)
        loss = torch.nn.CrossEntropyLoss()
        self.lr_schedule = torch.optim.lr_scheduler.MultiStepLR(optimizer, milestones=[400000, 600000], gamma=0.1)

        # Initialize the parent class
        super(GoAgent, self).__init__(name, policy_net, DQNGo(height, width, channel, outputs), optimizer, loss,
                                      gamma=gamma, batch_size=batch_size, tgt_update_thres=tgt_update_thres,
                                      mem_size=mem_size, config_file=config_file)

    def get_action(self, obs_dict, mode):
        """
        Given the current state of the environment and the simulation mode, choose the next action to perform.
        :param obs_dict: A dictionary containing observations from the environment, as well as a mask for illegal
        actions.
        :param mode: The mode in which the simulation is being run.
        :return:
        """

        # Increase the number of moves played
        self.nb_moves += 1

        # Extract required information from observation dictionary
        obs = obs_dict['observation'].unsqueeze(0)

        # Gather then network's output
        with torch.no_grad():
            self.q_net.eval()  # Put the network in evaluation mode to avoid any issue with regularization layers
            logits = self.q_net.forward(obs)

        if mode == Mode.TEST:
            # If the simulation is in test mode we use a greedy policy
            return logits.max(1)[1].view((1,))
        else:
            # Otherwise sample and return the next action
            return torch.multinomial(logits, num_samples=1, replacement=False)

    def update(self, state, action, next_state, reward, done):
        # Let the parent do the heavy-lifting
        loss_value = super().update(state, action, next_state, reward, done)

        # Update the learning rate schedule
        if loss_value is not None:
            self.lr_schedule.step()

        # Return the loss value for monitoring purposes
        return loss_value


class KataGoAgent(Agent):
    """
    Implement an agent that uses the KataGo controller to play the game of Go.
    """

    def __init__(self, name, boardsize, komi):

        # Initialize the parent class
        super().__init__(name)

        # Instantiate a controller to send KataGo commands
        katago_model = KATAGO_DIR.joinpath('g170e-b20c256x2-s5303129600-d1228401921.bin.gz')
        self.ctrl = KataGoController(model_path=katago_model)
        self.ctrl.set_boardsize(boardsize)
        self.ctrl.set_komi(komi)

        # Define additional required properties
        self.board_size = boardsize
        self.color = name.split('_')[0]
        self.opponent_color = 'black' if self.color == 'white' else 'white'
        # Following GTP specification 'I' cannot be used as a column name
        available_columns = ascii_uppercase.replace('I', '')
        self.cols = available_columns[0:boardsize]
        # Following GTP conventions the bottom row is labelled 1
        self.rows = list(map(lambda i: str(i), range(boardsize, 0, -1)))

    def reset(self):
        """
        Reset the board, move history, and number of captured stones.
        :return: A boolean flag indicating whether the operation succeeded.
        """

        return self.ctrl.reset()

    def set_name(self, name):
        """
        Set the agent's name and recompute the ours and the opponent's color.
        :param name: A string indicating the new name.
        :return: Nothing.
        """

        self.name = name
        self.color = name.split('_')[0]
        self.opponent_color = 'black' if self.color == 'white' else 'white'

    def close(self):
        self.ctrl.close()

    def get_action(self, observations, mode):
        """
        Extract the relevant observations and return the next best move.
        :param observations:
        :param mode: An instance of the Mode enumeration. This is not relevant here since KataGo has already been
        optimized and does not learn anymore.
        :return: An integer representing the next action to perform given the current state of the game,
         according to KataGo.
        """

        # Increase the number of moves played
        self.nb_moves += 1

        # Transform the observation into a numpy array
        obs = observations['observation'].numpy(force=True)

        # Extract the opponent's last move if any
        diff = obs[0] - obs[3]
        if np.any(diff != 0):
            last_move = diff.argmax()
            last_move = np.unravel_index(last_move, diff.shape)

            # Send the opponent's last move to KataGo
            vertex = ''.join([self.cols[last_move[1]], self.rows[last_move[0]]])
            self.ctrl.play(self.opponent_color, vertex)
        else:
            # Assume the opponent passed their turn
            if np.any(obs[0] != 0) and np.any(obs[1] != 0):
                self.ctrl.play(self.opponent_color, 'pass')

        # Request KataGo to generate the next move
        _, next_move = self.ctrl.genmove(self.color)

        # Translate the answer to something PettingZoo will understand
        # PettingZoo's Go engine does not implement the 'resign' action, so simply 'pass'
        if next_move in ['pass', 'resign']:
            return torch.tensor(self.board_size ** 2, device=PT_DEVICE)
        else:
            col = self.cols.index(next_move[0])
            row = self.rows.index(next_move[1:])
            return torch.tensor(row * self.board_size + col, device=PT_DEVICE)

    def update(self):
        """
        The model used for KataGo is a frozen sent of parameters. It has already been optimized. Hence, it does not
        need to learn anymore.
        :return: Nothing
        """
        pass

    def save_config(self, file_path):
        """
        As a separate process and engine, KataGo manages its own internal state. Therefore, this function does nothing.
        :param file_path: Not applicable.
        :return: Nothing.
        """
        pass

    def load_config(self, file_path):
        """
        As a separate process and engine, KataGo manages its own internal state. Therefore, this function does nothing.
        :param file_path: Not applicable.
        :return: Nothing.
        """
        pass


class KataGoPersonalityAgent(Agent):
    """
    Implement an agent which uses KataGo as its source of policy,
    but uses a personality vector to influence the action selection process.
    """

    def __init__(self, personalities, boardsize, komi):
        """
        Initialize the parent class and set parameters required for action selection.
        :param personalities: A dictionary of 3-tuples of integers representing the agents' personalities
        based on Mehrabian's PAD-scale.
        :param boardsize: An integer representing the size of the board.
        :param komi: A float representing how much handicap is given to the white stones for being the second player.
        """

        for personality in personalities.values():
            assert len(personality) == 3, \
                'The personality vector should contain three values corresponding to: Pleasure, Arousal and Dominance.'
            assert all([-1 <= e <= 1 for e in personality]), \
                'The components of the personality vector should all be in the range [-1, 1].'

        # Initialize parent class
        super().__init__(name='KataGo_n_Personality')

        # Set various parameters required for the decision making process
        katago_model = KATAGO_DIR.joinpath('g170e-b20c256x2-s5303129600-d1228401921.bin.gz')
        self._ctrl = KataGoController(model_path=katago_model)
        self._ctrl.set_boardsize(boardsize)
        self._ctrl.set_komi(komi)
        self._boardsize = boardsize
        # TODO: Store different personalities for Black and White
        self._personalities = personalities

        # Following GTP specification 'I' cannot be used as a column name
        available_columns = ascii_uppercase.replace('I', '')
        self._cols = available_columns[0:boardsize]
        # Following GTP conventions the bottom row is labelled 1
        self._rows = list(map(lambda i: str(i), range(boardsize, 0, -1)))

    def get_action(self, color, prev_action):
        """
        Select the next action to perform for the stones of the given color. The selection process involves different
        influences from the agent's personality.
        :param color: A string representing the color of the stone whose turn it is to select an action.
        :param prev_action: An integer representing the action selected by the previous player.
        :return: An integer representing the next move to play.
        """

        # Get the raw policy
        raw_policy = np.array(self._ctrl.get_policy_slow()[1])

        # Get the player's personality
        try:
            personality = self._personalities[color]
            pleasure, arousal, dominance = personality
        except Exception as e:
            self._ctrl.close()
            raise e

        # Add dirichlet noise based on the level of arousal.
        # Arousal: high -noise, low +noise
        arousal = convert_range(arousal, -1, 1, 0, 1e-3)
        personal_policy = np.where(raw_policy > 0,
                                   raw_policy + np.random.normal(loc=0, scale=1, size=362) * (1e-3 - arousal),
                                   np.nan)

        # Extract all legal moves
        is_move_legal = personal_policy != np.nan

        if prev_action != -1 and prev_action != self._boardsize ** 2:
            prev_act_coord = idx2coord(prev_action, self._boardsize)
            distances = []
            for tmp_act in range(self._boardsize ** 2):
                # Skip computation for illegal moves
                if tmp_act == prev_action or not is_move_legal[tmp_act]:
                    distances.append(np.nan)
                else:
                    # Extract the coordinates corresponding to the move
                    tmp_act_coord = idx2coord(tmp_act, self._boardsize)
                    # Compute the distance between the current solution and the previous player's move
                    distances.append(distance.euclidean(prev_act_coord, tmp_act_coord))
            # This is the distance corresponding to the 'pass' move which is made inversely proportional to its
            # probability
            distances.append(convert_range(personal_policy[-1], 0, 1,
                                           distance.euclidean([0, 0], [self._boardsize - 1, self._boardsize - 1]), 0))
            distances = np.array(distances)
        else:
            distances = 1 / personal_policy

        # Combine the probabilities and distances into scores
        dominance = convert_range(dominance, -1, 1, 0, 1)
        scores = dominance * personal_policy + (1 - dominance) / distances
        # Turn all NaNs into infinitely big negative numbers so that their corresponding score is always at the bottom
        np.nan_to_num(scores, copy=False, nan=-np.inf)

        # Sort the scores
        sorted_scores = np.flip(np.argsort(scores))

        # Compute the index based on pleasure
        nb_legal_moves = len(personal_policy[is_move_legal])
        if nb_legal_moves > 10:
            # Choose within the top 10 moves
            pleasure = int(convert_range(pleasure, -1, 1, 0, 9))
        else:
            # Choose best to worst within available moves
            pleasure = int(convert_range(pleasure, -1, 1, 0, nb_legal_moves-1))

        # Return the action which "best" represent the player's personality
        return sorted_scores[pleasure]

    def play(self, color, idx):
        """
        Notify KataGo that a new stone has been placed on the board, along with the stone's color.
        :param color: A string representing the color of the stone, and by extension the player, who made the move.
        :param idx: An integer representing the vertex on which the stone was place.
        :return: Nothing.
        """

        # Transform the index into a GTP formatted vertex
        if idx == (self._boardsize ** 2):
            vertex = 'pass'
        else:
            row, col = idx2coord(idx, self._boardsize)
            vertex = ''.join([self._cols[col], self._rows[row]])

        # Log the moves played by the different stones for debugging purposes
        logger.debug(f'{color} played: {vertex}')

        # Let KataGo what stone has been place on the board and where
        self._ctrl.play(color, vertex)

    def reset(self):
        """
        Clear the board and the moves history.
        :return: Nothing.
        """
        self._ctrl.reset()

    def close(self):
        """
        Request the underlying KataGo process to terminate as gracefully as possible.
        :return: Nothing.
        """
        self._ctrl.close()

    def update(self):
        pass

    def save_config(self, file_path):
        pass

    def load_config(self, file_path):
        pass


class Lc0PersonalityAgent(Agent):
    """
    Define an agent whose decision-making process is influenced by personality.
    This agent takes its initial policy from Leela Chess Zero. As such, it is only suited for playing Chess.
    """

    def __init__(self, personality, nn_size='m'):
        """
        Initialize the agent and define its level of pleasure, arousal and dominance.
        :param personality: A tuple whose components correspond the agent's level of pleasure, arousal and dominance.
        :param nn_size: A character indicating the size of network to use for the underlying controller.
        """

        # Initialize the parent class
        name = f'lc0_perso_{"_".join(map(lambda x: str(round(x, 2)), personality))}'
        super().__init__(name)

        # Instantiate the controller which will interact with the underlying Lc0 architecture
        self._lc0 = Lc0Controller(nn_size=nn_size)

        # Record the level of pleasure, arousal and dominance
        self._pleasure = convert_range(personality[0], -1, 1, 0, 1)
        self._arousal = convert_range(personality[1], -1, 1, 0, 1)
        self._dominance = convert_range(personality[2], -1, 1, 0, 0.5)

    @property
    def personality(self):
        return self._pleasure, self._arousal, self._dominance

    @personality.setter
    def personality(self, personality):
        self._pleasure = convert_range(personality[0], -1, 1, 0, 1)
        self._arousal = convert_range(personality[1], -1, 1, 0, 1)
        self._dominance = convert_range(personality[2], -1, 1, 0, 0.5)

    def update(self, move):
        """
        Update the game state given a new move.
        :param move: A string representing the action taken on the board.
        """

        # Simply send the information to the controller
        self._lc0.play(move)

    def get_action(self):
        """
        Retrieve the next action to perform based on the opponent's last move.
        Note that this process is influenced by the personality given to each instance.
        :return: A string corresponding to the next action to perform on the board.
        """

        # Get the initial policy from Lc0
        avail_moves, _, init_policy = self._lc0.get_policy()

        if len(init_policy) == 0:
            return None

        # Add noise depending on arousal
        init_policy = np.array(init_policy)
        policy = np.random.normal(0, 1, size=init_policy.shape[0]) * (1 - self._arousal) + init_policy

        # Compute the distance between the opponent's last move and all available moves
        opp_move = self._lc0.last_move
        if opp_move is None:
            distances = 1 / policy
        else:
            distances = []
            opp_coord = move2coord(opp_move)
            for move in avail_moves:
                coord = move2coord(move)
                if coord == opp_coord:  # We are taking the opponent's piece
                    distances.append(1e-8)  # Avoid divide by zero
                else:
                    distances.append(distance.euclidean(opp_coord, coord))
            distances = np.array(distances)

        # Use dominance as a threshold for ignoring distance
        perso_policy = policy * self._dominance + (1 - self._dominance) / distances

        # Select and return the final move using pleasure
        sorted_idx = np.flip(np.argsort(perso_policy))
        mv_idx = sorted_idx[int(self._pleasure * (len(avail_moves) - 1))]
        return avail_moves[mv_idx]

    def reset(self):
        """
        Reset the game's state and move history.
        """
        self._lc0.reset()

    def save_config(self, file_path):
        pass

    def load_config(self, file_path):
        pass


class Lc0EmoAgent(Agent):
    """
    Defines an agent whose policy results from a mix between Leela Chess Zero's knowledge and emotions.
    Since the initial policy is provided by Leela Chess Zero, this agent is only capable of playing chess.
    """

    def __init__(self, name, lr_rate, nn_size='m', policy_size=1858, emo_size=8, train=True):
        """
        Initialize the underlying lc0 controller, the AutoEncoder used to obtain the emotionally influenced policy, and
        any property required for both training and playing.
        :param nn_size: A character from the set {s, m, l} indicating the size of the network to use for the underlying
        lc0 controller.
        :param policy_size: An integer corresponding to the number of possible moves.
        :param emo_size: An integer indicating the size of the emotion space.
        :param train: A flag indicating the mode in which to put the underlying network.
        """

        # Initialize the parent class
        super().__init__(f'lc0_emo_{name}')

        # Initialize both lc0's controller and the AutoEncoder
        self._lc0 = Lc0Controller(nn_size=nn_size)
        self._net = EmoChessAE(policy_size=policy_size, emo_size=emo_size)
        self._net.train(mode=train)

        # Initialize the loss for the different heads
        self._policy_loss = nn.MSELoss()
        self._emo_loss = nn.MSELoss()

        # Initialize the rest of the required properties
        self._optimizer = torch.optim.Adam(self._net.parameters(), lr=lr_rate, amsgrad=True)
        self._policy_size = policy_size
        self._emo_size = emo_size
        self._prev_out = None
        self._prev_in = None
        self._prev_draw = None
        self._train = train

    def update(self, move, mv_idx):
        """
        Optimize the AutoEncoder and update Leela Chess Zero's board state.
        :param move: A string representing the last move played.
        :param mv_idx: An integer corresponding to the index of the lst move played.
        :return:
        """

        if self._train and self._prev_out is not None and self._prev_in is not None:
            # Get information about the score
            _, draw, _ = self._lc0.get_score_info()
            logger.info(f'Draw prob: {draw}')

            if self._prev_draw is not None:
                # Build the policy target to increase the probability of a draw
                targ_policy = torch.zeros(self._policy_size, dtype=torch.float32, device=PT_DEVICE)
                targ_policy[mv_idx] = self._prev_out[0][mv_idx] + self._prev_draw - draw

                # Optimize the network
                loss = self._policy_loss(self._prev_out[0], targ_policy)
                loss += self._emo_loss(self._prev_out[1], self._prev_in[1])
                self._optimizer.zero_grad(set_to_none=True)
                loss.backward(retain_graph=True)
                self._optimizer.step()
                logger.info(f'Training loss: {loss.item()}')
                self._prev_out = None

            # Store the previous draw probability
            self._prev_draw = draw

        # Send Leela Chess Zero the last move
        self._lc0.play(move)

    def get_action(self, emo_idx):
        """
        Retrieve the next move to perform based on the initial policy and the provided emotion vector.
        :param emo_idx: An integer representing the index corresponding to the emotion the agent is currently
        experiencing.
        :return: A tuple containing: a string representing the next action to perform and an index corresponding to the
        move.
        """

        # Get the initial policy from Leela Chess Zero
        avail_moves, move_indices, lc0_policy = self._lc0.get_policy()
        init_policy = torch.zeros(self._policy_size, dtype=torch.float32)
        init_policy[list(move_indices)] = torch.tensor(lc0_policy, dtype=torch.float32)

        # Build the emotion tensor
        emotion = torch.zeros(self._emo_size, dtype=torch.float32)
        emotion[emo_idx] = 1

        # Store the input for later updating
        self._prev_in = (init_policy.to(device=PT_DEVICE), emotion.to(device=PT_DEVICE))

        # Activate the AutoEncoder
        self._prev_out = self._net(init_policy, emotion)

        # Select and return the next move
        policy = self._prev_out[0].tolist()
        legal_policy = np.array([policy[idx] for idx in move_indices])
        if legal_policy.shape[0] != 0:
            mv_idx = legal_policy.argmax()
            return avail_moves[mv_idx], move_indices[mv_idx]
        else:
            return None, None

    def save_config(self, file_path):
        """
        Save the weights corresponding to the AutoEncoder.
        :param file_path: A string representing the relative path where to save the configuration.
        :return: Nothing.
        """
        torch.save(self._net.state_dict(), Path(file_path).expanduser().resolve())

    def load_config(self, file_path):
        """
        Load the weights in the underlying autoencoder.
        :param file_path: A string representing the path to the file from which to load the weights.
        :return: Nothing.
        """
        abs_file_path = Path(file_path).expanduser().resolve()
        if abs_file_path.exists():
            self._net.load_state_dict(torch.load(abs_file_path))
            self._net.eval()
            self._net.train(mode=self._train)
        else:
            logger.exception(f'Could not load the weights from: {abs_file_path}. File does not exist or you do not have'
                             f' the correct permission.')

    def reset(self):
        """
        Reinitialize the board's state, move history, and state of the AutoEncoder's LSTM.
        :return: Nothing.
        """
        self._lc0.reset()
        self._net.reset_lstm()
        self._prev_in = None
        self._prev_out = None
        self._prev_draw = None


class HumanAgent(Agent):
    """
    Defines an agent that takes its command from a human via the command line.
    """

    def __init__(self, action_space):
        """
        Initialize the parent class with a default name.
        :param action_space: A gymnasium.spaces.Discrete instance describing the available action space.
        """

        super(HumanAgent, self).__init__('Human')
        self.act_space = action_space

    def update(self, *args, **kwargs):
        pass

    def save_config(self, file_path):
        pass

    def load_config(self, file_path):
        pass

    def get_action(self, obs_dict, *args, **kwargs):
        """
        Read the action from the command line and send them directly to the environment.
        :param obs_dict: TODO
        :return: A tensor representing the action chosen by the human in charge.
        """

        # Increase the number of moves played
        self.nb_moves += 1

        action_mask = obs_dict['action_mask']

        start, end = self.act_space.start, (self.act_space.start + self.act_space.n) - 1

        act = None
        while act is None:
            try:
                act = int(input(f'Please select an integer between {start} and '
                                f'{end}: '))

                # Detect illegal moves
                if action_mask[act].item() == 0 or act < start or act > end:
                    act = None
                    logger.error('Illegal move')
            except ValueError:
                act = None

        return torch.tensor([act], device=PT_DEVICE)


class RandomAgent(Agent):
    """
    Define an agent with a random policy.
    """

    def __init__(self, name, action_space):
        """
        Store the agent's action space.
        :param name: A string identifying a specific instance of an agent.
        :param action_space: An instance of gymnasium.space.Space, defining the space of possible actions.
        """

        super(RandomAgent, self).__init__(name)
        self._avail_acts = action_space

    def update(self):
        """
        This is an agent with a random policy, there is no need for learning.
        Therefore, this method does nothing
        :return: Nothing.
        """
        pass

    def get_action(self, obs_dict, *args, **kwargs):
        """
        Randomly sample the agent's available actions.
        :param obs_dict: TODO
        :return: A random action.
        """

        # Increase the number of moves played
        self.nb_moves += 1

        # Return a random action sampled over the population of legal actions
        return torch.tensor([torch.distributions.Categorical(probs=obs_dict['action_mask']).sample()], device=PT_DEVICE)

    def save_config(self, file_path):
        """
        A random agent does not have any internal state. Therefore, there is nothing to save to disk.
        Consequently, this method does nothing.
        :param file_path: The path to the file where the configuration should be saved.
        :return: Nothing.
        """
        pass

    def load_config(self, file_path):
        """
        A random agent does not have any internal state. As such, there is nothing to load from disk.
        Consequently, this method does nothing.
        :param file_path: The path to the file from where the configuration should be loaded.
        :return: Nothing.
        """
        pass


class ConnFourAgent(DQNAgent):
    """
    Defines an agent specifically for playing the game of connect four as described in:
    https://pettingzoo.farama.org/environments/classic/connect_four/
    This specific agent uses an epsilon-greedy policy to decide on the next action to perform.
    """

    def __init__(self, name, q_net, tgt_net, l_rate=1e-4, eps_min=0.05, eps_max=0.9, eps_decay=200, gamma=0.9,
                 batch_size=128, tgt_update_thres=10, mem_size=100000, config_file=None):
        """
        Initialize the Policy and Target network to fit the constraints of the specific Go environment.
        :param name: A string identifying a specific instance of an agent.
        :param q_net: An instance of torch.nn.Module which will be trained to play the game of connect four.
        :param tgt_net: An instance of torch.nn.Module which is used during the training phase to provide stable
        expected values.
        :param l_rate: A float representing the rate at which network parameters are adapted during learning.
        :param eps_min: A float representing the minimal/final probability with which a random action will be selected
        by the epsilon-greedy policy.
        :param eps_max: A float representing the maximal/initial probability with which a random action will be selected
         by the epsilon-greedy policy.
        :param eps_decay: A float roughly corresponding to the number of update steps over which epsilon will decrease
        from its maximal to its minimal value.
        :param gamma: A float representing how future reward are weighted relative to short-term ones.
        :param batch_size: An integer defining how many data point to sample for training the agent on each loop.
        :param tgt_update_thres: An integer representing the number of learning steps to wait before updating the
        target network's weights.
        :param mem_size: An integer used as the upper bond on the size of the memory for the agent.
        :param config_file: A pathlib.Path to the file containing the initial values to be used for the policy
        network's parameters.
        """

        # Instantiate a new optimizer and loss for the policy network
        # For connect four the observation space is fixed and known in advance
        # The same goes for the size of the output layer, which is equal to the width of the observation space
        policy_net = q_net  # DQNConn4(6, 7, 2, 7)
        optimizer = optim.Adam(q_net.parameters(), lr=l_rate)
        loss = torch.nn.HuberLoss()

        # Initialize the parent class
        super(ConnFourAgent, self).__init__(name, q_net, tgt_net, optimizer, loss,
                                            gamma=gamma, batch_size=batch_size, tgt_update_thres=tgt_update_thres,
                                            mem_size=mem_size, config_file=config_file)

        # Save the parameters for the epsilon-greedy policy
        self.eps_min = eps_min
        self.eps_max = eps_max
        self.eps_decay = eps_decay

    def get_action(self, obs_dict, mode):
        """
        Given the current state of the environment and the simulation mode, choose the next action to perform.
        :param obs_dict: A dictionary containing observations from the environment, as well as a mask for illegal
        actions.
        :param mode: The mode in which the simulation is being run.
        :return:
        """

        # Increase the number of moves played
        self.nb_moves += 1

        # Extract required information from observation dictionary
        obs = obs_dict['observation'].unsqueeze(0)

        # Gather then network's output
        with torch.no_grad():
            self.q_net.eval()  # Put the network in evaluation mode to avoid any issue with regularization layers
            logits = self.q_net.forward(obs)

        # Compute the new threshold for selecting random action
        eps_thres = self.eps_min + (self.eps_max - self.eps_min) * math.exp(-1. * self.step_cnt / self.eps_decay)

        # Return an action according to the epsilon-greedy policy
        # Unless the simulation is in training mode. In which case we use a greedy policy
        if random() > eps_thres or mode == Mode.TEST:
            # Return the best action
            return logits.max(1)[1].view((1,))
        else:
            return torch.tensor([choice(range(7))], device=PT_DEVICE)


class C4A0Agent(ConnFourAgent):
    """
    TODO
    """

    def __init__(self, name, l_rate=1e-4, eps_min=0.05, eps_max=0.9, eps_decay=200, gamma=0.9,
                 batch_size=128, tgt_update_thres=10, mem_size=100000, config_file=None):
        """
        Initialize the Policy and Target network to fit the constraints of the specific Go environment.
        :param name: A string identifying a specific instance of an agent.
        :param l_rate: A float representing the rate at which network parameters are adapted during learning.
        :param eps_min: A float representing the minimal/final probability with which a random action will be selected
        by the epsilon-greedy policy.
        :param eps_max: A float representing the maximal/initial probability with which a random action will be selected
         by the epsilon-greedy policy.
        :param eps_decay: A float roughly corresponding to the number of update steps over which epsilon will decrease
        from its maximal to its minimal value.
        :param gamma: A float representing how future reward are weighted relative to short-term ones.
        :param batch_size: An integer defining how many data point to sample for training the agent on each loop.
        :param tgt_update_thres: An integer representing the number of learning steps to wait before updating the
        target network's weights.
        :param mem_size: An integer used as the upper bond on the size of the memory for the agent.
        :param config_file: A pathlib.Path to the file containing the initial values to be used for the policy
        network's parameters.
        """

        # Initialize the parent class with DQNC4A0 networks and specific name
        super().__init__(f'A0_{name}', DQNC4A0(6, 7, 2, 7), DQNC4A0(6, 7, 2, 7), l_rate, eps_min, eps_max, eps_decay,
                         gamma, batch_size, tgt_update_thres, mem_size, config_file)


class C4A1Agent(ConnFourAgent):
    """
    TODO
    """

    def __init__(self, name, l_rate=1e-4, eps_min=0.05, eps_max=0.9, eps_decay=200, gamma=0.9,
                 batch_size=128, tgt_update_thres=10, mem_size=100000, config_file=None):
        """
        Initialize the Policy and Target network to fit the constraints of the specific Go environment.
        :param name: A string identifying a specific instance of an agent.
        :param l_rate: A float representing the rate at which network parameters are adapted during learning.
        :param eps_min: A float representing the minimal/final probability with which a random action will be selected
        by the epsilon-greedy policy.
        :param eps_max: A float representing the maximal/initial probability with which a random action will be selected
         by the epsilon-greedy policy.
        :param eps_decay: A float roughly corresponding to the number of update steps over which epsilon will decrease
        from its maximal to its minimal value.
        :param gamma: A float representing how future reward are weighted relative to short-term ones.
        :param batch_size: An integer defining how many data point to sample for training the agent on each loop.
        :param tgt_update_thres: An integer representing the number of learning steps to wait before updating the
        target network's weights.
        :param mem_size: An integer used as the upper bond on the size of the memory for the agent.
        :param config_file: A pathlib.Path to the file containing the initial values to be used for the policy
        network's parameters.
        """

        # Initialize the parent class with DQNC4A0 networks and specific name
        super().__init__(f'A1_{name}', DQNC4A1(6, 7, 2, 7), DQNC4A1(6, 7, 2, 7), l_rate, eps_min, eps_max, eps_decay,
                         gamma, batch_size, tgt_update_thres, mem_size, config_file)


class C4A2Agent(ConnFourAgent):
    """
    TODO
    """

    def __init__(self, name, l_rate=1e-4, eps_min=0.05, eps_max=0.9, eps_decay=200, gamma=0.9,
                 batch_size=128, tgt_update_thres=10, mem_size=100000, config_file=None):
        """
        Initialize the Policy and Target network to fit the constraints of the specific Go environment.
        :param name: A string identifying a specific instance of an agent.
        :param l_rate: A float representing the rate at which network parameters are adapted during learning.
        :param eps_min: A float representing the minimal/final probability with which a random action will be selected
        by the epsilon-greedy policy.
        :param eps_max: A float representing the maximal/initial probability with which a random action will be selected
         by the epsilon-greedy policy.
        :param eps_decay: A float roughly corresponding to the number of update steps over which epsilon will decrease
        from its maximal to its minimal value.
        :param gamma: A float representing how future reward are weighted relative to short-term ones.
        :param batch_size: An integer defining how many data point to sample for training the agent on each loop.
        :param tgt_update_thres: An integer representing the number of learning steps to wait before updating the
        target network's weights.
        :param mem_size: An integer used as the upper bond on the size of the memory for the agent.
        :param config_file: A pathlib.Path to the file containing the initial values to be used for the policy
        network's parameters.
        """

        # Initialize the parent class with DQNC4A0 networks and specific name
        super().__init__(f'A2_{name}', DQNC4A2(6, 7, 2, 7), DQNC4A2(6, 7, 2, 7), l_rate, eps_min, eps_max, eps_decay,
                         gamma, batch_size, tgt_update_thres, mem_size, config_file)


class C4B0Agent(ConnFourAgent):
    """
    TODO
    """

    def __init__(self, name, l_rate=1e-4, eps_min=0.05, eps_max=0.9, eps_decay=200, gamma=0.9,
                 batch_size=128, tgt_update_thres=10, mem_size=100000, config_file=None):
        """
        Initialize the Policy and Target network to fit the constraints of the specific Go environment.
        :param name: A string identifying a specific instance of an agent.
        :param l_rate: A float representing the rate at which network parameters are adapted during learning.
        :param eps_min: A float representing the minimal/final probability with which a random action will be selected
        by the epsilon-greedy policy.
        :param eps_max: A float representing the maximal/initial probability with which a random action will be selected
         by the epsilon-greedy policy.
        :param eps_decay: A float roughly corresponding to the number of update steps over which epsilon will decrease
        from its maximal to its minimal value.
        :param gamma: A float representing how future reward are weighted relative to short-term ones.
        :param batch_size: An integer defining how many data point to sample for training the agent on each loop.
        :param tgt_update_thres: An integer representing the number of learning steps to wait before updating the
        target network's weights.
        :param mem_size: An integer used as the upper bond on the size of the memory for the agent.
        :param config_file: A pathlib.Path to the file containing the initial values to be used for the policy
        network's parameters.
        """

        # Initialize the parent class with DQNC4A0 networks and specific name
        super().__init__(f'B0_{name}', DQNC4B0(6, 7, 2, 7), DQNC4B0(6, 7, 2, 7), l_rate, eps_min, eps_max, eps_decay,
                         gamma, batch_size, tgt_update_thres, mem_size, config_file)


class C4B1Agent(ConnFourAgent):
    """
    TODO
    """

    def __init__(self, name, l_rate=1e-4, eps_min=0.05, eps_max=0.9, eps_decay=200, gamma=0.9,
                 batch_size=128, tgt_update_thres=10, mem_size=100000, config_file=None):
        """
        Initialize the Policy and Target network to fit the constraints of the specific Go environment.
        :param name: A string identifying a specific instance of an agent.
        :param l_rate: A float representing the rate at which network parameters are adapted during learning.
        :param eps_min: A float representing the minimal/final probability with which a random action will be selected
        by the epsilon-greedy policy.
        :param eps_max: A float representing the maximal/initial probability with which a random action will be selected
         by the epsilon-greedy policy.
        :param eps_decay: A float roughly corresponding to the number of update steps over which epsilon will decrease
        from its maximal to its minimal value.
        :param gamma: A float representing how future reward are weighted relative to short-term ones.
        :param batch_size: An integer defining how many data point to sample for training the agent on each loop.
        :param tgt_update_thres: An integer representing the number of learning steps to wait before updating the
        target network's weights.
        :param mem_size: An integer used as the upper bond on the size of the memory for the agent.
        :param config_file: A pathlib.Path to the file containing the initial values to be used for the policy
        network's parameters.
        """

        # Initialize the parent class with DQNC4A0 networks and specific name
        super().__init__(f'B1_{name}', DQNC4B1(6, 7, 2, 7), DQNC4B1(6, 7, 2, 7), l_rate, eps_min, eps_max, eps_decay,
                         gamma, batch_size, tgt_update_thres, mem_size, config_file)


class C4B2Agent(ConnFourAgent):
    """
    TODO
    """

    def __init__(self, name, l_rate=1e-4, eps_min=0.05, eps_max=0.9, eps_decay=200, gamma=0.9,
                 batch_size=128, tgt_update_thres=10, mem_size=100000, config_file=None):
        """
        Initialize the Policy and Target network to fit the constraints of the specific Go environment.
        :param name: A string identifying a specific instance of an agent.
        :param l_rate: A float representing the rate at which network parameters are adapted during learning.
        :param eps_min: A float representing the minimal/final probability with which a random action will be selected
        by the epsilon-greedy policy.
        :param eps_max: A float representing the maximal/initial probability with which a random action will be selected
         by the epsilon-greedy policy.
        :param eps_decay: A float roughly corresponding to the number of update steps over which epsilon will decrease
        from its maximal to its minimal value.
        :param gamma: A float representing how future reward are weighted relative to short-term ones.
        :param batch_size: An integer defining how many data point to sample for training the agent on each loop.
        :param tgt_update_thres: An integer representing the number of learning steps to wait before updating the
        target network's weights.
        :param mem_size: An integer used as the upper bond on the size of the memory for the agent.
        :param config_file: A pathlib.Path to the file containing the initial values to be used for the policy
        network's parameters.
        """

        # Initialize the parent class with DQNC4B2 networks and specific name
        super().__init__(f'B2_{name}', DQNC4B2(6, 7, 2, 7), DQNC4B2(6, 7, 2, 7), l_rate, eps_min, eps_max, eps_decay,
                         gamma, batch_size, tgt_update_thres, mem_size, config_file)
