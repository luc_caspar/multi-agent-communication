#!/usr/bin/env python3
"""
A module containing the definition of different neural networks for use by various agents.
"""
import torch
import torch.nn as nn
from settings import PT_DEVICE


def conv2d_size_out(size, kernel_size=3, stride=1, padding=0):
    """
    Compute the output size of a 2D convolutional layer.
    :param size: Either the height or width of the input.
    :param kernel_size: The size of the kernel used for the convolution.
    :param stride: The stride used for the convolution.
    :param padding: An integer representing how much padding is added to all four sides.
    :return: The size of the output along a single dimension.
    """

    # The formula is directly lifted from PyTorch documentation:
    # https://pytorch.org/docs/stable/generated/torch.nn.Conv2d.html#torch.nn.Conv2d
    return ((size + 2 * padding - kernel_size) // stride) + 1


class EmoChessAE(nn.Module):
    """
    Define a multi-head AutoEncoder, which takes Emotion and policy as input and reproduces both emotion and policy on
    separate heads.
    The policy head is used to decide on the next action.
    """

    def __init__(self, policy_size=1858, emo_size=8):
        """
        TODO
        :param policy_size: An integer corresponding to the number of possible moves.
        :param emo_size: An integer representing the number of observable emotions.
        """

        # Initialize the parent class
        super(EmoChessAE, self).__init__()

        # Initialize Hidden and Cell states for the LSTM
        # More details here: https://pytorch.org/docs/stable/generated/torch.nn.LSTM.html#torch.nn.LSTM
        self._lh = torch.zeros((1, 800), device=PT_DEVICE, dtype=torch.float32)
        self._lc = torch.zeros((1, 800), device=PT_DEVICE, dtype=torch.float32)

        # Declare embedding layers for emotions
        # Only 3 output features since most dimensional theories seem to agree on that number of dimensions
        self._e_embed = nn.Sequential(
            nn.Linear(in_features=emo_size, out_features=3, device=PT_DEVICE),
            nn.Sigmoid()  # This could be replaced with a ReLU as well
        )

        # Declare the common blocks
        self._enc = nn.Sequential(
            nn.Linear(in_features=3 + policy_size, out_features=1331, device=PT_DEVICE),
            nn.ReLU(),
            nn.Linear(in_features=1331, out_features=800, device=PT_DEVICE),
            nn.ReLU()
        )
        self._bn = nn.LSTM(input_size=800, hidden_size=800, num_layers=1, device=PT_DEVICE)
        self._dec = nn.Sequential(
            nn.Linear(in_features=800, out_features=1331, device=PT_DEVICE),
            nn.ReLU()
        )

        # Declare the various decoders
        self._p_dec = nn.Sequential(
            nn.Linear(in_features=1331, out_features=policy_size, device=PT_DEVICE),
            nn.Softmax(dim=0)
        )
        self._e_dec = nn.Sequential(
            nn.Linear(in_features=1331, out_features=3, device=PT_DEVICE),
            nn.ReLU(),
            nn.Linear(in_features=3, out_features=emo_size, device=PT_DEVICE),
            nn.Sigmoid()  # This could be replaced with a ReLU as well
        )

    def reset_lstm(self):
        """
        Reset the hidden and cell states of the LSTM stack.
        :return: Nothing.
        """
        self._lh = torch.zeros((1, 800), device=PT_DEVICE, dtype=torch.float32)
        self._lc = torch.zeros((1, 800), device=PT_DEVICE, dtype=torch.float32)

    def forward(self, policy, emotion):
        """
        TODO
        :param policy:
        :param emotion:
        :return: Reconstructed emotion and policy vectors.
        """

        # Ensure the input is on the same device as the network
        policy = policy.to(device=PT_DEVICE)
        emotion = emotion.to(device=PT_DEVICE)

        # Compute the policy and emotion embeddings
        e_e = self._e_embed(emotion)

        # Activate the common blocks
        embed = torch.cat([policy, e_e])
        x = self._enc(embed)
        # Transform the encoder's output into a sequence of a single element
        # TODO: Initialize a history of all encoder output to feed to the lstm?
        x, (self._lh, self._lc) = self._bn(x.unsqueeze(dim=0), (self._lh.detach(), self._lc.detach()))

        # Compute the output of both decoders based on the last element in the LSTM output
        x = self._dec(x[-1])
        p_out = self._p_dec(x)
        e_out = self._e_dec(x)

        # Return the reconstructed policy and emotion vectors
        return p_out, e_out


class DQNConn4(nn.Module):
    """
    Define a Deep Q-Network to be used by a DQNAgent to play the game of connect four.
    """

    def __init__(self, height, width, channel, outputs):
        """
        Initialize the layers of the CNN.
        :param height: An integer representing the input's height.
        :param width: An integer representing the input's width.
        :param channel: An integer representing the number of channels.
        :param outputs: An integer representing the number of outputs.
        """

        # Initialize the parent class
        super(DQNConn4, self).__init__()

        # Define the different layers used in the network
        self.conv1 = nn.Sequential(nn.Conv2d(in_channels=channel, out_channels=8*channel, kernel_size=3, stride=1,
                                             padding=1, device=PT_DEVICE),
                                   nn.ReLU(),
                                   nn.BatchNorm2d(8 * channel, device=PT_DEVICE))
        self.conv2 = nn.Sequential(nn.Conv2d(in_channels=8*channel, out_channels=16*channel, kernel_size=3, stride=1,
                                             padding=1, device=PT_DEVICE),
                                   nn.ReLU(),
                                   nn.BatchNorm2d(16*channel, device=PT_DEVICE))
        self.conv3 = nn.Sequential(nn.Conv2d(in_channels=16*channel, out_channels=16*channel, kernel_size=3, stride=1,
                                             padding=1, device=PT_DEVICE),
                                   nn.ReLU(),
                                   nn.BatchNorm2d(16*channel, device=PT_DEVICE))
        self.conv4 = nn.Sequential(nn.Conv2d(in_channels=16*channel, out_channels=16*channel, kernel_size=3, stride=1,
                                             padding=1, device=PT_DEVICE),
                                   nn.ReLU(),
                                   nn.BatchNorm2d(16*channel, device=PT_DEVICE))

        self.lin1 = nn.Sequential(nn.Linear(in_features=height * width * (channel * 16), out_features=12 * outputs,
                                            bias=True, device=PT_DEVICE),
                                  nn.ReLU())
        self.lin2 = nn.Sequential(nn.Linear(in_features=12 * outputs, out_features=6 * outputs, bias=True,
                                            device=PT_DEVICE),
                                  nn.ReLU())
        # Since connect four uses an epsilon-greedy policy, the last layer could also use a Tanh activation
        self.head = nn.Sequential(nn.Linear(in_features=6 * outputs, out_features=outputs, bias=True, device=PT_DEVICE),
                                  nn.Softmax(dim=1))

    def forward(self, x):
        """
        Define the forward pass for the network, transforming an input tensor into predictions.
        :param x: An input tensor.
        :return: Predictions corresponding to the available actions.
        """

        # Copy the input tensor to the appropriate device
        x = self.conv1(x)
        x = self.conv2(x)
        x = self.conv3(x)
        x = self.conv4(x)
        x = torch.flatten(x, start_dim=1)  # May need adjustment to avoid flattening the batch dimension
        x = self.lin1(x)
        x = self.lin2(x)

        return self.head(x)


class DQNC4A0(nn.Module):
    """
    TODO
    """

    def __init__(self, height, width, channel, outputs):
        """
        Initialize the layers of the CNN.
        :param height: An integer representing the input's height.
        :param width: An integer representing the input's width.
        :param channel: An integer representing the number of channels.
        :param outputs: An integer representing the number of outputs.
        """

        # Initialize the parent class
        super(DQNC4A0, self).__init__()

        # Define the different layers used in the network
        self.conv1 = nn.Sequential(nn.Conv2d(in_channels=channel, out_channels=64, kernel_size=4, stride=1,
                                             padding='same', device=PT_DEVICE),
                                   nn.ReLU(),
                                   nn.BatchNorm2d(64, device=PT_DEVICE))

        self.conv2 = nn.Sequential(nn.Conv2d(in_channels=64, out_channels=64, kernel_size=2, stride=1,
                                             padding='same', device=PT_DEVICE),
                                   nn.ReLU(),
                                   nn.BatchNorm2d(64, device=PT_DEVICE))

        self.conv3 = nn.Sequential(nn.Conv2d(in_channels=64, out_channels=64, kernel_size=2, stride=1,
                                             padding='same', device=PT_DEVICE),
                                   nn.ReLU(),
                                   nn.BatchNorm2d(64, device=PT_DEVICE))

        self.lin1 = nn.Sequential(nn.Linear(in_features=height * width * 64, out_features=64,
                                            bias=True, device=PT_DEVICE),
                                  nn.ReLU())

        self.head = nn.Sequential(nn.Linear(in_features=64, out_features=outputs, bias=True, device=PT_DEVICE),
                                  nn.Softmax(dim=1))

    def forward(self, x):
        """
        Define the forward pass for the network, transforming an input tensor into predictions.
        :param x: An input tensor.
        :return: Predictions corresponding to the available actions.
        """

        # Copy the input tensor to the appropriate device
        x = self.conv1(x)
        x = self.conv2(x)
        x = self.conv3(x)
        x = torch.flatten(x, start_dim=1)
        x = self.lin1(x)

        return self.head(x)


class DQNC4A1(nn.Module):
    """
    A copy of DQNC4A0 but with Dropout layers between convolutional blocks.
    """

    def __init__(self, height, width, channel, outputs):
        """
        Initialize the layers of the CNN.
        :param height: An integer representing the input's height.
        :param width: An integer representing the input's width.
        :param channel: An integer representing the number of channels.
        :param outputs: An integer representing the number of outputs.
        """

        # Initialize the parent class
        super(DQNC4A1, self).__init__()

        # Define the different layers used in the network
        self.conv1 = nn.Sequential(nn.Conv2d(in_channels=channel, out_channels=64, kernel_size=4, stride=1,
                                             padding='same', device=PT_DEVICE),
                                   nn.ReLU(),
                                   nn.BatchNorm2d(64, device=PT_DEVICE),
                                   nn.Dropout(p=0.2))

        self.conv2 = nn.Sequential(nn.Conv2d(in_channels=64, out_channels=64, kernel_size=2, stride=1,
                                             padding='same', device=PT_DEVICE),
                                   nn.ReLU(),
                                   nn.BatchNorm2d(64, device=PT_DEVICE),
                                   nn.Dropout(p=0.2))

        self.conv3 = nn.Sequential(nn.Conv2d(in_channels=64, out_channels=64, kernel_size=2, stride=1,
                                             padding='same', device=PT_DEVICE),
                                   nn.ReLU(),
                                   nn.BatchNorm2d(64, device=PT_DEVICE))

        self.lin1 = nn.Sequential(nn.Linear(in_features=height * width * 64, out_features=64,
                                            bias=True, device=PT_DEVICE),
                                  nn.ReLU())

        self.head = nn.Sequential(nn.Linear(in_features=64, out_features=outputs, bias=True, device=PT_DEVICE),
                                  nn.Softmax(dim=1))

    def forward(self, x):
        """
        Define the forward pass for the network, transforming an input tensor into predictions.
        :param x: An input tensor.
        :return: Predictions corresponding to the available actions.
        """

        # Copy the input tensor to the appropriate device
        x = self.conv1(x)
        x = self.conv2(x)
        x = self.conv3(x)
        x = torch.flatten(x, start_dim=1)
        x = self.lin1(x)

        return self.head(x)


class DQNC4A2(nn.Module):
    """
    A copy of DQNC4A1 but with LeakyReLU activation instead of ReLU.
    """

    def __init__(self, height, width, channel, outputs):
        """
        Initialize the layers of the CNN.
        :param height: An integer representing the input's height.
        :param width: An integer representing the input's width.
        :param channel: An integer representing the number of channels.
        :param outputs: An integer representing the number of outputs.
        """

        # Initialize the parent class
        super(DQNC4A2, self).__init__()

        # Define the different layers used in the network
        self.conv1 = nn.Sequential(nn.Conv2d(in_channels=channel, out_channels=64, kernel_size=4, stride=1,
                                             padding='same', device=PT_DEVICE),
                                   nn.LeakyReLU(),
                                   nn.BatchNorm2d(64, device=PT_DEVICE),
                                   nn.Dropout(p=0.2))

        self.conv2 = nn.Sequential(nn.Conv2d(in_channels=64, out_channels=64, kernel_size=2, stride=1,
                                             padding='same', device=PT_DEVICE),
                                   nn.LeakyReLU(),
                                   nn.BatchNorm2d(64, device=PT_DEVICE),
                                   nn.Dropout(p=0.2))

        self.conv3 = nn.Sequential(nn.Conv2d(in_channels=64, out_channels=64, kernel_size=2, stride=1,
                                             padding='same', device=PT_DEVICE),
                                   nn.LeakyReLU(),
                                   nn.BatchNorm2d(64, device=PT_DEVICE))

        self.lin1 = nn.Sequential(nn.Linear(in_features=height * width * 64, out_features=64,
                                            bias=True, device=PT_DEVICE),
                                  nn.LeakyReLU())

        self.head = nn.Sequential(nn.Linear(in_features=64, out_features=outputs, bias=True, device=PT_DEVICE),
                                  nn.Softmax(dim=1))

    def forward(self, x):
        """
        Define the forward pass for the network, transforming an input tensor into predictions.
        :param x: An input tensor.
        :return: Predictions corresponding to the available actions.
        """

        # Copy the input tensor to the appropriate device
        x = self.conv1(x)
        x = self.conv2(x)
        x = self.conv3(x)
        x = torch.flatten(x, start_dim=1)
        x = self.lin1(x)

        return self.head(x)


class DQNC4B0(nn.Module):
    """
    TODO
    """

    def __init__(self, height, width, channel, outputs):
        """
        Initialize the layers of the CNN.
        :param height: An integer representing the input's height.
        :param width: An integer representing the input's width.
        :param channel: An integer representing the number of channels.
        :param outputs: An integer representing the number of outputs.
        """

        # Initialize the parent class
        super(DQNC4B0, self).__init__()

        # Define the different layers used in the network
        self.conv1 = nn.Sequential(nn.Conv2d(in_channels=channel, out_channels=128, kernel_size=4, stride=1,
                                             padding='same', device=PT_DEVICE),
                                   nn.ReLU(),
                                   nn.BatchNorm2d(128, device=PT_DEVICE))

        self.lin1 = nn.Sequential(nn.Linear(in_features=height * width * 128, out_features=64,
                                            bias=True, device=PT_DEVICE),
                                  nn.ReLU())

        self.lin2 = nn.Sequential(nn.Linear(in_features=64, out_features=64, bias=True, device=PT_DEVICE), nn.ReLU())

        self.head = nn.Sequential(nn.Linear(in_features=64, out_features=outputs, bias=True, device=PT_DEVICE),
                                  nn.Softmax(dim=1))

    def forward(self, x):
        """
        Define the forward pass for the network, transforming an input tensor into predictions.
        :param x: An input tensor.
        :return: Predictions corresponding to the available actions.
        """

        # Copy the input tensor to the appropriate device
        x = self.conv1(x)
        x = torch.flatten(x, start_dim=1)
        x = self.lin1(x)
        x = self.lin2(x)

        return self.head(x)


class DQNC4B1(nn.Module):
    """
    A copy of DQNC4B0 but with a dropout layer at the end of the convolutional block.
    """

    def __init__(self, height, width, channel, outputs):
        """
        Initialize the layers of the CNN.
        :param height: An integer representing the input's height.
        :param width: An integer representing the input's width.
        :param channel: An integer representing the number of channels.
        :param outputs: An integer representing the number of outputs.
        """

        # Initialize the parent class
        super(DQNC4B1, self).__init__()

        # Define the different layers used in the network
        self.conv1 = nn.Sequential(nn.Conv2d(in_channels=channel, out_channels=128, kernel_size=4, stride=1,
                                             padding='same', device=PT_DEVICE),
                                   nn.ReLU(),
                                   nn.BatchNorm2d(128, device=PT_DEVICE),
                                   nn.Dropout(p=0.2))

        self.lin1 = nn.Sequential(nn.Linear(in_features=height * width * 128, out_features=64,
                                            bias=True, device=PT_DEVICE),
                                  nn.ReLU())

        self.lin2 = nn.Sequential(nn.Linear(in_features=64, out_features=64, bias=True, device=PT_DEVICE), nn.ReLU())

        self.head = nn.Sequential(nn.Linear(in_features=64, out_features=outputs, bias=True, device=PT_DEVICE),
                                  nn.Softmax(dim=1))

    def forward(self, x):
        """
        Define the forward pass for the network, transforming an input tensor into predictions.
        :param x: An input tensor.
        :return: Predictions corresponding to the available actions.
        """

        # Copy the input tensor to the appropriate device
        x = self.conv1(x)
        x = torch.flatten(x, start_dim=1)
        x = self.lin1(x)
        x = self.lin2(x)

        return self.head(x)


class DQNC4B2(nn.Module):
    """
    A copy of DQNC4B1 but with LeakyReLU activation rather than simple ReLU.
    """

    def __init__(self, height, width, channel, outputs):
        """
        Initialize the layers of the CNN.
        :param height: An integer representing the input's height.
        :param width: An integer representing the input's width.
        :param channel: An integer representing the number of channels.
        :param outputs: An integer representing the number of outputs.
        """

        # Initialize the parent class
        super(DQNC4B2, self).__init__()

        # Define the different layers used in the network
        self.conv1 = nn.Sequential(nn.Conv2d(in_channels=channel, out_channels=128, kernel_size=4, stride=1,
                                             padding='same', device=PT_DEVICE),
                                   nn.LeakyReLU(),
                                   nn.BatchNorm2d(128, device=PT_DEVICE),
                                   nn.Dropout(p=0.2))

        self.lin1 = nn.Sequential(nn.Linear(in_features=height * width * 128, out_features=64,
                                            bias=True, device=PT_DEVICE),
                                  nn.LeakyReLU())

        self.lin2 = nn.Sequential(nn.Linear(in_features=64, out_features=64, bias=True, device=PT_DEVICE),
                                  nn.LeakyReLU())

        self.head = nn.Sequential(nn.Linear(in_features=64, out_features=outputs, bias=True, device=PT_DEVICE),
                                  nn.Softmax(dim=1))

    def forward(self, x):
        """
        Define the forward pass for the network, transforming an input tensor into predictions.
        :param x: An input tensor.
        :return: Predictions corresponding to the available actions.
        """

        # Copy the input tensor to the appropriate device
        x = self.conv1(x)
        x = torch.flatten(x, start_dim=1)
        x = self.lin1(x)
        x = self.lin2(x)

        return self.head(x)


class ResidualBlock(nn.Module):
    """
    Define a block of layers with a residual connection surrounding it.
    """

    def __init__(self, in_channels, out_channels, kernel_size=3, stride=1):
        """
        Specify the layer structure of the residual block.
        :param in_channels: An integer representing the number of input channels for the internal convolutions.
        :param out_channels: An integer representing the number of output channels for the internal convolutions.
        :param kernel_size: An integer or tuple representing the size of the kernel to use for the internal
        convolutions.
        :param stride: An integer or tuple representing the stride to use for the internal convolutions.
        """

        # Initialize the parent class
        super(ResidualBlock, self).__init__()

        # Define the layers that make up the residual block
        self._bn1 = nn.BatchNorm2d(in_channels, device=PT_DEVICE)
        self._conv1 = nn.Conv2d(in_channels, out_channels, kernel_size=kernel_size, stride=stride, padding=1,
                                bias=False, device=PT_DEVICE)
        self._bn2 = nn.BatchNorm2d(out_channels, device=PT_DEVICE)
        self._conv2 = nn.Conv2d(in_channels, out_channels, kernel_size=kernel_size, stride=stride, padding=1,
                                bias=False, device=PT_DEVICE)
        self._relu = nn.ReLU()

    def forward(self, x):
        """
        Define the forward pass across the block's layers.
        :param x: A tensor representing a single or a batch or inputs.
        :return: A tensor representing the block output.
        """

        # Keep a reference to the input tensor for the residual connection
        residual = x

        # Go through the forward pass
        out = x
        out = self._conv1(out)
        out = self._bn1(out)
        out = self._relu(out)
        out = self._conv2(out)
        out = self._bn2(out)
        # Combine the output with the residual
        out += residual
        out = self._relu(out)

        return out


class DQNGo(nn.Module):
    """
    Define a Deep Q-Network to be used by DQNAgents to play the game of Go.
    """

    def __init__(self, height, width, channel, outputs, residual_tower_size=10):
        """
        Specify the network's architecture.
        It should be noted that the definition is a copy from the original AlphaZero paper from Silver et al. (2017),
        where only the policy head has been retained and everything has been half-sized.
        :param height: An integer representing the input's height.
        :param width: An integer representing the input's width.
        :param channel: An integer representing the number of channels.
        :param outputs: An integer representing the number of outputs.
        :param residual_tower_size: An integer representing the number of residual blocks to include in
        the architecture.
        """

        # Initialize the parent class
        super(DQNGo, self).__init__()

        # First convolution block
        self._conv1 = nn.Sequential(
            nn.Conv2d(in_channels=channel, out_channels=128, kernel_size=3, stride=1, padding=1, device=PT_DEVICE, bias=False),
            nn.BatchNorm2d(128, device=PT_DEVICE),
            nn.ReLU()
        )

        # Second the residual tower
        self._res = nn.Sequential(*[ResidualBlock(in_channels=128,
                                                  out_channels=128,
                                                  kernel_size=3,
                                                  stride=1) for _ in range(residual_tower_size)])

        # Finally the policy head
        self._conv2 = nn.Sequential(
            nn.Conv2d(in_channels=128, out_channels=2, kernel_size=1, stride=1, padding=0, device=PT_DEVICE, bias=False),
            nn.BatchNorm2d(2, device=PT_DEVICE),
            nn.ReLU()
        )
        # Because of the specific kernel size, stride, and padding used for the different convolutions
        # (including the residual blocks) the output dimensions stay the same. Only the number of channel increases at
        # the start. This makes it easier to compute the number of input features for the linear layer.
        self.head = nn.Sequential(
            nn.Flatten(),
            nn.Linear(in_features=(width * height * 2), out_features=outputs, device=PT_DEVICE),
            nn.Softmax(dim=1)
        )

    def forward(self, x):
        """
        Define the forward pass for the network, transforming an input tensor into predictions.
        :param x: An input tensor.
        :return: Predictions corresponding to the available actions.
        """

        x = self._conv1(x)
        x = self._res(x)
        x = self._conv2(x)

        # Return the policy head which corresponds to the logits probabilities for all actions
        return self.head(x)
