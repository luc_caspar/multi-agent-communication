#!/usr/bin/env python3
from enum import Enum
from collections import namedtuple, deque
from random import sample
from string import ascii_lowercase

# Define a transition from the current state to the next by taking an action
Transitions = namedtuple('Transitions', ('state', 'action', 'next_state', 'reward', 'done'))


class ReplayMemory(object):
    """
    Define a way to more easily manage an agent's memory.
    """

    def __init__(self, max_size=10000):
        """
        Initialize the agent's memory as a queue to preserve chronological order.
        :param max_size: An integer limiting the size of the memory.
        """
        self.memory = deque(maxlen=max_size)

    def __len__(self):
        """
        The length of the ReplayMemory is the same as the length of the underlying queue.
        :return: The length of the underlying queue.
        """
        return len(self.memory)

    def push(self, state, action, next_state, reward, done):
        """
        Add a transition to the memory.
        :param state: A tensor representing the agent's current state.
        :param action: A tensor representing the action the agent performed to transition between states.
        :param next_state: A tensor representing the agent's new state.
        :param reward: A tensor representing the reward perceived by the agent when transitioning between states.
        :param done: A boolean flag indicating whether the game was terminated as a result of that transition.
        :return: Nothing.
        """
        self.memory.append(Transitions(state, action, next_state, reward, done))

    def sample(self, batch_size=1):
        """
        Samples a random batch of transitions from the agent's memory.
        :param batch_size: An integer representing the size of the batch to sample.
        :return: A tensor containing a batch of memories.
        """
        return sample(self.memory, batch_size)


class Mode(Enum):
    TRAIN = 'train'
    TEST = 'test'


class Score(Enum):
    NULL = 'N'
    WIN = 'W'
    LOSE = 'L'
    ILLEGAL = 'I'
    TIE = 'T'


def convert_range(value, min_r1, max_r1, min_r2, max_r2):
    """
    Convert a value from one range to another.
    :param value: The number to be converted. Either float or int will work.
    :param min_r1: The lower bound of the first interval.
    :param max_r1: The upper bound of the first interval.
    :param min_r2: The lower bound of the second interval.
    :param max_r2: The upper bound of the second interval.
    :return:
    """

    assert max_r1 != min_r1 and max_r2 != min_r2, 'Both ends of an interval cannot be equal.'

    return (((value - min_r1) * (max_r2 - min_r2)) / (max_r1 - min_r1)) + min_r2


def idx2coord(idx, boardsize):
    """
    Transform an index in the range [0, N*N] into a set of coordinates.
    :param idx: An integer in the range [0, N*N].
    :param boardsize: An integer representing the number of elements in a row,
    as well as the number of row in a column.
    :return: A tuple of integers corresponding to the element's coordinates.
    """

    return idx // boardsize, idx % boardsize


def move2coord(mv_str):
    """
    Convert a UCI formatted move into coordinates.
    :param mv_str: A string representing a move.
    :return: A tuple X, Y of coordinates corresponding to the tile on which the move ends.
    """

    if mv_str == "0000":
        # This is a Null move
        return -1, -1

    if len(mv_str) == 5:
        # Extract destination tile
        dest = mv_str[-3:-1]

    else:
        # Extract destination tile
        dest = mv_str[-2:]

    # Compute and return the coordinates
    return ascii_lowercase.index(dest[0]), int(dest[1])
