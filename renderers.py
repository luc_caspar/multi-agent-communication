#!/usr/bin/env python3
from abc import ABCMeta
from threading import Thread
from queue import Queue, Empty
from pathlib import Path
from io import BytesIO
from string import ascii_lowercase
import numpy as np
import cv2 as cv
import pygame
from cairosvg import svg2png
from PIL import Image
from chess import Board, Move, square
import chess.svg
from time import sleep


def svg2bytes(svg_img):
    """
    Convert the SVG image given in parameter into a PNG image.
    :param svg_img: An object representing a SVG image.
    :return: A BytesIO memory location in which the image's content is stored in PNG format.
    """

    # Declare the memory in which the array will be stored
    mem = BytesIO()

    # Convert the SVG image to PNG
    svg2png(bytestring=svg_img, write_to=mem)

    return mem


def svg2np(svg_img):
    """
    Convert the SVG image given in parameter into a numpy array.
    :param svg_img: An object representing a SVG image.
    :return: A numpy array.
    """

    # Convert the image to png and store it into memory
    mem = svg2bytes(svg_img)

    # Convert the png image to a numpy array and return it
    return np.array(Image.open(mem))


class Renderer(Thread):
    """
    Define an abstract class which all renderer should implement.
    """

    def __init__(self):
        super().__init__()
        self.q_in = Queue()
        self.q_out = Queue()


class VideoRenderer(Renderer):
    """
    Define a class who's only purpose is to render a video using frames that are given on its input queue.
    """

    def __init__(self, file_path, height, width, fps, fourcc, is_color):
        """
        TODO
        :param file_path:
        :param height:
        :param width:
        :param fps:
        :param fourcc:
        :param is_color:
        """

        super().__init__()

        # Delete the output queue which will not be used here
        del self.q_out

        # Instantiate a Video Writer to render the video from frames
        self._vw = cv.VideoWriter()
        file_path = Path(file_path).expanduser().resolve()
        self._vw.open(str(file_path), fourcc, fps, (width, height), is_color)

    def write(self, frame):
        self._vw.write(frame)

    def close(self):
        self._vw.release()


class ChessVideoRenderer(VideoRenderer):
    """
    Define a class to render videos using chess board states.
    """

    def __init__(self, file_path, size, fps, fourcc, is_color):
        """
        TODO
        :param file_path:
        :param size:
        :param fps:
        :param fourcc:
        :param is_color:
        """

        super().__init__(file_path, size, size, fps, fourcc, is_color)
        self._board = Board()
        self._video_size = size

    def run(self) -> None:
        """
        Ingest moves from the input pipe and render the resulting board states as a video.
        """

        # Render the initial board state
        svg_frame = chess.svg.board(board=self._board, size=self._video_size)
        np_frame = svg2np(svg_frame)
        self.write(cv.cvtColor(np_frame, cv.COLOR_RGB2BGR))

        try:
            old_move = None
            while True:
                # Wait for a new move to arrive
                move = self.q_in.get()

                # Close the thread if the game is done
                if move is None:
                    self.q_in.task_done()
                    break

                # Update the board
                move = Move.from_uci(move)
                self._board.push(move)

                # Render the next frame based on the new board state
                svg_frame = chess.svg.board(board=self._board, lastmove=old_move, size=self._video_size)
                np_frame = svg2np(svg_frame)
                self.write(cv.cvtColor(np_frame, cv.COLOR_RGB2BGR))

                # Record the move and signal the task as done
                old_move = move
                self.q_in.task_done()
        finally:
            self.q_in.join()
            self.close()


class ChessGuiRenderer(Renderer):
    """
    Define a GUI which uses pyglet to allow a human to play against an artificial agent.
    """

    def __init__(self, size):

        super().__init__()
        self._size = size

    def close(self):
        self.q_out.put(None)
        pygame.quit()
        while not self.q_in.empty():
            self.q_in.get()
            self.q_in.task_done()
        self.q_in.join()

    def run(self) -> None:
        # Initialize pygame and variables required for tracking the state of the game/board
        pygame.init()
        clock = pygame.time.Clock()
        screen = pygame.display.set_mode((self._size, self._size))
        old_move = None
        move = None
        board = Board()
        hl_squares = {}
        mv_src = None
        # Compute properties related to the board displayed on screen
        border_size = (self._size / 100) * 4
        square_size = (self._size - 2 * border_size) / 8

        try:
            while True:
                # Handle keyboard and click events
                for evt in pygame.event.get(eventtype=[pygame.QUIT, pygame.KEYDOWN, pygame.MOUSEBUTTONDOWN], pump=True):
                    # Press Q to quit the game
                    if evt.type == pygame.QUIT or evt.type == pygame.KEYDOWN and evt.key == pygame.K_q:
                        raise StopIteration()
                    elif evt.type == pygame.MOUSEBUTTONDOWN and evt.button == pygame.BUTTON_LEFT:
                        # Compute the rank and file of a mouse event on the board
                        x, y = evt.pos
                        x = int((x - border_size) // square_size)
                        y = int(8 - ((y - border_size) // square_size))
                        if mv_src is None:
                            # Record the origin of a move
                            mv_src = square(x, y - 1)
                            hl_squares = {mv_src: '#ff0000'}
                        else:
                            # Compute the destination square from the coordinates
                            mv_dst = square(x, y - 1)
                            if mv_src != mv_dst:
                                # TODO: Handle piece Promotion
                                # Create the move and send it to the engine
                                self.q_out.put(Move(from_square=mv_src, to_square=mv_dst).uci())
                            mv_src, mv_dst = None, None
                            hl_squares = {}
                else:
                    try:
                        old_move = move
                        # Get the next move to display from the engine
                        move = self.q_in.get(block=False)
                        self.q_in.task_done()
                        if move is None:
                            raise StopIteration()
                        # Update the board
                        move = Move.from_uci(move)
                        board.push(move)
                    except Empty:
                        pass

                    # Display the new board state
                    svg_frame = chess.svg.board(board, lastmove=old_move, size=self._size,
                                                fill=hl_squares)
                    png_img = Image.open(svg2bytes(svg_frame))
                    pg_img = pygame.image.fromstring(png_img.tobytes(), png_img.size, 'RGB')
                    pg_img_rect = pg_img.get_rect()

                    screen.blit(pg_img, pg_img_rect)
                    pygame.display.flip()

                    # Cap the refresh rate to 60 FPS
                    clock.tick(60)

        except StopIteration:
            pass
        finally:
            # Always gracefully terminate the renderer
            self.close()
