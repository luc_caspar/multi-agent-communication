#!/usr/bin/env python3
import pickle
import requests
from functools import partial
from datetime import datetime, timedelta
import pandas as pd
from pettingzoo.classic import go_v5
from pettingzoo.utils.env_logger import EnvLogger
import cv2 as cv
from agents import KataGoPersonalityAgent
from settings import VIDEO_DIR, DATA_DIR
from utils import convert_range

# Disable logging from pettingzoo's Go environment
EnvLogger.suppress_output()

# Declare game related parameters
BOARDSIZE = 19
KOMI = 7.5


def get_nuka_personality():
    """
    Retrieve the Nukabot's sensor readings from either the cache file or the cloud,
    then translate them into a personality vector.
    :return: A tuple whose values correspond to Pleasure, Arousal and Dominance.
    """

    # Define some of the parameters required for fetching the personality
    root_url = 'http://infinityloops.xyz/nuka/voice/data-req.php'
    cache_file = DATA_DIR.joinpath('nuka_personality.pkl')

    # Measures only change every 24 hours so no need to fetch new ones if it has not been that long since the last run
    if cache_file.exists():
        try:
            # Load the cached data
            with cache_file.open('rb') as f:
                cache = pickle.load(f)
        except Exception:
            cache = {}
    else:
        cache = {}

    # Refresh the cache if necessary
    now = datetime.now()
    if len(cache) == 0 or now - cache['time'] > timedelta(hours=24):
        cache['time'] = now
        for label, req_type in zip(['orp', 'ph', 'nitrate'],
                                   ['orp_median', 'ph_median', 'gas_NO2_median']):

            r = requests.get(f'{root_url}?reqType={req_type}')
            if r.ok:
                cache[label] = float(r.json()['body'])

        # Write the new value to file
        with cache_file.open('wb') as f:
            pickle.dump(cache, f, protocol=pickle.HIGHEST_PROTOCOL)

    # Make sure we did get some values from the NukaBot
    assert len(cache) != 0, "Could not retrieve sensor readings from NukaBot."

    # Map and return the sensor readings to a personality vector
    return sensor2perso(cache['ph'], cache['orp'], cache['nitrate'])


def sensor2perso(ph, orp, nitrate):
    """
    TODO
    :param ph:
    :param orp:
    :param nitrate:
    :return:
    """
    # Compute the distance between the current pH level and the optimal level
    ph = abs(4.4 - ph)

    # Map the sensor readings to Mehrabian's PAD scale
    pleasure = convert_range(ph, 0, 4.4, 1, -1)  # The farther the pH is from 4.4 the worse the pleasure
    arousal = convert_range(orp, -352, 734, -1, 1)  # High values of ORP mean high arousal
    dominance = convert_range(nitrate, 0, 0.35, 1, -1)  # The lower NO2 is the higher the dominance

    # Return the personality
    return pleasure, arousal, dominance


def main(pers_black, pers_white, video_fname):
    """
    TODO
    :param pers_black:
    :param pers_white:
    :param video_fname:
    :return:
    """

    # Initialize the environment
    env = go_v5.env(komi=KOMI, board_size=BOARDSIZE, render_mode='rgb_array')
    env.reset()

    # Initialize a video writer
    video_writer = cv.VideoWriter()
    fps = env.metadata['render_fps']
    fourcc = cv.VideoWriter.fourcc(*'XVID')
    frame = env.render()
    frame = cv.cvtColor(frame, cv.COLOR_RGB2BGR)
    w, h, c = frame.shape
    is_color = c > 1
    video_writer.open(str(VIDEO_DIR.joinpath(video_fname)), fourcc, fps, (h, w), is_color)
    video_writer.write(frame)  # This writes the first frame

    # Initialize the KataGo agent with personalities
    personalities = {'black': pers_black, 'white': pers_white}
    player = KataGoPersonalityAgent(personalities, BOARDSIZE, KOMI)

    # Play the game
    try:
        act = -1
        for player_name in env.agent_iter(max_iter=362 * 2):
            # Extract the player's color
            player_color = player_name.split('_')[0]

            # Check if the agent has been terminated or truncated
            _, _, terminated, truncated, _ = env.last()

            # Let the agent play the next move if possible
            if terminated or truncated:
                env.step(None)
            else:
                act = player.get_action(player_color, act)
                env.step(act)
                player.play(player_color, act)  # This is to update KataGo's internal state

            # Render the next video frame
            frame = env.render()
            frame = cv.cvtColor(frame, cv.COLOR_RGB2BGR)
            video_writer.write(frame)
    finally:
        # Gracefully terminate the video stream and KataGo
        video_writer.release()
        player.close()


if __name__ == "__main__":
    # Fetch and use NukaBot's personality
    # nuka_personality = get_nuka_personality()
    # nuka_perso_name = '_'.join(map(str, map(partial(round, ndigits=2), nuka_personality)))
    # personalities = {'optimal': (-1, 1, 1), nuka_perso_name: get_nuka_personality()}

    # Extract the different moods over the last 30 days
    df = pd.read_csv('Data/nukabot_data_set_01_20230114-0214.csv')
    df['created_at'] = pd.to_datetime(df['created_at'], yearfirst=True)
    df = df[['ph', 'orp', 'gas_NO2', 'created_at']].set_index('created_at')
    df = df.to_period(freq='D')  # Transform datetime into date. So get rid of time.

    # Compute the median taking only non-zero values into account
    ph = df[df['ph'] != 0]['ph']
    orp = df[df['orp'] != 0]['orp']
    no2 = df[df['gas_NO2'] != 0]['gas_NO2']
    daily_median_ph = ph.groupby('created_at').median()
    daily_median_orp = orp.groupby('created_at').median()
    daily_median_no2 = no2.groupby('created_at').median()

    # Let the optimal individual play against each version of Nukabot
    p_white = 'optimal'
    for ph, orp, no2 in zip(daily_median_ph, daily_median_orp, daily_median_no2):
        nuka_personality = sensor2perso(ph, orp, no2)
        p_name_black = '.'.join(map(str, map(partial(round, ndigits=2), nuka_personality)))
        print(f'{p_name_black} against {p_white}')
        video_fname = f'kgp_b_{p_name_black}_w_{p_white}.avi'
        main(nuka_personality, (-1, 1, 1), video_fname)
