#!/usr/bin/env python3
from pathlib import Path
from argparse import ArgumentParser
from numpy.random import uniform, randint
from chess import Board, Move
from loguru import logger
from agents import Lc0EmoAgent, Lc0PersonalityAgent

# Define a simple command line interface
parser = ArgumentParser()
parser.add_argument('-e', '--epoch', dest='epochs', type=int, required=True,
                    help='The number of epochs (or games in this case) to train the agent on.')
parser.add_argument('--weight_folder', dest='weight_folder', type=str, required=True,
                    help='The relative path to the directory in which to save the checkpoints.')
parser.add_argument('-lr', '--learning_rate', dest='lr', type=float, default=0.001,
                    help='Specify the learning rate to use during training.')
parser.add_argument('--weight_file', dest='weight_path', type=str,
                    help='The relative path to the file from which to load weights for the AutoEncoder.', default='')
parser.add_argument('--log', dest='log_file', type=str, required=True,
                    help='The relative path of the log file.')

args = parser.parse_args()

# Initialize the logging facility
logger.add(Path(args.log_file).expanduser().resolve(),
           format='{time:YYYY-MM-DD HH:mm:ss} | {level} | {message}', level='INFO')

# Declare the path to the directory for storing the checkpoints
weight_folder = Path(args.weight_folder)
if not weight_folder.exists():
    raise RuntimeError('The directory provided to store the checkpoints does not exist or you do not have the correct'
                       ' permission.')

# Instantiate two agents
white = Lc0EmoAgent(name='white', lr_rate=args.lr, nn_size='s')
black = Lc0PersonalityAgent(personality=(-1, 1, 1), nn_size='s')

for epoch in range(args.epochs):
    # Instantiate a new chess board
    board = Board()

    # Play a game of chess
    cnt = 0
    outcome = board.outcome()
    while outcome is None:
        # Rest the emotion and move index
        act_idx = None
        # Play the next move
        if cnt % 2 == 0:
            action, act_idx = white.get_action(emo_idx=randint(8))
        else:
            action = black.get_action()

        # The last player could not or did not move
        if action is None:
            break

        # Update both players
        white.update(action, act_idx)
        black.update(action)
        board.push(Move.from_uci(action))

        # Increment the number of moves played so far
        cnt += 1

        # Check the games outcome
        outcome = board.outcome()

    # Let the user know about the outcome
    logger.info(f'Game ended with outcome: {outcome.termination}, '
                f'the winner is {"white" if outcome.winner else "black"} with a score of: {outcome.result()}')

    # Make a checkpoint if necessary
    if epoch % 10 == 0:
        white.save_config(weight_folder.joinpath(f'lc0_emo_ae_{epoch}.pth'))

    # Reset the different agents
    white.reset()
    black.reset()

    # Assign a new personality to the second player
    # black.personality = uniform(-1, 1, 3).tolist()

# Make a final checkpoint
white.save_config(weight_folder.joinpath(f'lc0_emo_ae_final.pth'))
