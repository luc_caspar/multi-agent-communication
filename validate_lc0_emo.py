#!/usr/bin/env python3
from argparse import ArgumentParser
from pathlib import Path
from numpy.random import randint
from torch import no_grad
from chess import Board, Move
from loguru import logger
from agents import Lc0EmoAgent
from renderers import ChessGuiRenderer

# Declare a simple command line interface
parser = ArgumentParser()
parser.add_argument('-f', '--weight_file', dest='weight_file', type=str, required=True,
                    help='The relative path to the file from which to load the network weights.')
parser.add_argument('--log', dest='log_file', type=str, required=True,
                    help='The relative path of the log file.')

args = parser.parse_args()

# Initialize the logging facility
logger.add(Path(args.log_file).expanduser().resolve(),
           format='{time:YYYY-MM-DD HH:mm:ss} | {level} | {message}', level='INFO')

# Initiate the renderer
render = ChessGuiRenderer(size=800)
render.start()

# Initialize the chess board
board = Board()

# Instantiate the Emotional agent
black = Lc0EmoAgent('lc0_emo', lr_rate=0, nn_size='s', train=False)
weight_path = Path(args.weight_file).expanduser().resolve()
if weight_path.exists() and weight_path.is_file():
    black.load_config(weight_path)

# Play a game of chess
try:
    cnt = 0
    outcome = board.outcome()
    mv_idx = None
    while outcome is None:
        # Play the next move
        if cnt % 2 == 0:
            move = render.q_out.get()
            render.q_out.task_done()
        else:
            with no_grad():
                move, mv_idx = black.get_action(randint(8))

        # The last player could not or did not move
        if move is None:
            outcome = board.outcome()
            break

        # Update the board state
        board.push(Move.from_uci(move))
        render.q_in.put(move)
        black.update(move=move, mv_idx=mv_idx)

        # Increment the move counter and check the game's outcome
        cnt += 1
        outcome = board.outcome()

    if outcome is not None:
        logger.info(f'Game ended in {outcome.termination}, winner is {"white" if outcome.winner else "black"} '
                    f'with a score of {outcome.result()}')

finally:
    # Gracefully terminate the renderer
    render.q_in.put(None)
    while not render.q_out.empty():
        render.q_out.get()
        render.q_out.task_done()
    render.q_out.join()
    render.join()
