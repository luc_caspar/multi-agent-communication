#!/usr/bin/env python3
from pathlib import Path
from argparse import ArgumentParser
from collections import defaultdict
import torch
from pettingzoo.utils.env_logger import EnvLogger
import pandas as pd
import cv2 as cv
from loguru import logger
from tqdm import tqdm
from utils import Mode, Score
from settings import PT_DEVICE, VIDEO_DIR


def file_type(path_str):
    """
    Make sure that the given argument corresponds to an existing and readable file.
    :param path_str: A string representing a relative path to a file.
    :return: A string representing the absolute path to the file.
    """

    file_path = Path(path_str).expanduser().resolve()
    if file_path.exists():
        if file_path.is_file():
            return str(file_path)
        else:
            raise TypeError('The config_file argument expected a path to a file, but a directory path was '
                            'provided.')
    else:
        raise ValueError('File not found. The file either does not exists, or you do not have permission to read '
                         'it.')


class Cli(object):
    """
    Define the arguments common to all command line interfaces (CLI) used in this project.
    """

    def __init__(self, train_func, test_func, desc=None):
        """
        Initialize the argument parser. Then declare the commonly used arguments.
        """
        super(Cli, self).__init__()

        # Define a command line interface
        self.parser = ArgumentParser(description=desc)

        sub_parsers = self.parser.add_subparsers(dest='cmd',
                                                 description='Sub-commands for executing the environment '
                                                             'in different modes.',
                                                 required=True)

        # Define sub-commands
        self.sub_cmd = {'train': sub_parsers.add_parser('train', aliases=['tr'],
                                                        help='Initialize the environment in training mode, with an '
                                                             'independent agent for each player.'),
                        'test': sub_parsers.add_parser('test', aliases=['te'],
                                                       help='Initialize the environment in testing mode, with an '
                                                            'independent agent for each player.')}

        # Define global arguments
        for parser in self.sub_cmd.values():
            parser.add_argument('-e', '--epochs', dest='epochs', type=int,
                                help='The number of epochs to train/test the agents for.',
                                default=1e4)
            parser.add_argument('-i', '--iterations', dest='max_iter', type=int,
                                help='The maximum number of iterations a single game should run for.',
                                default=9*9*2)
            parser.add_argument('--config_file', dest='config_files', action='append', type=file_type,
                                help='A file containing the weights and biases to initialize an agent.')

        # Define arguments specific to each sub-command
        # Train
        train = self.sub_cmd['train']
        train.set_defaults(func=train_func)  # This is the default function to execute for the train sub-command
        train.add_argument('--self-play', dest='self_play', action='store_true',
                           help='Initialize the environment in training mode, but with a single agent for all '
                                'players.')
        train.add_argument('--config_save_thres', dest='epoch_thres', type=int,
                           help="""How many epochs to wait between saving the agent's configuration to file.""",
                           default=1000)
        train.add_argument('-lr', '--learning_rate', dest='lr_rate', type=float,
                           help='The rate at which network parameters are adjusted during training.',
                           default=1e-2)

        # Test
        test = self.sub_cmd['test']
        test.set_defaults(func=test_func)  # This is the default function to execute for the test sub-command
        ex_grp = test.add_mutually_exclusive_group()
        ex_grp.add_argument('--headless', dest='headless', action='store_true',
                            help='Prevents the program from trying to display the current game. However, the video is '
                                 'still rendered.')
        ex_grp.add_argument('--human', dest='human', action='store_true',
                            help='Turn one of the player into a human agent. This enables a human to select the next '
                                 'action to perform from the command line.')
        test.add_argument('--random', dest='random', action='store_true',
                          help='Turn one of the player into a random agent. By definition a random agent selects the '
                               'next action to perform by randomly samply it over the space of available actions.')
        test.add_argument('--no-video', dest='video', action='store_false',
                          help='Prevents the program from trying to render a video of the current environment.')

    def add_mutually_exclusive_group(self, *args, **kwargs):
        groups = []
        if 'cmd' in kwargs:
            cmd = self.sub_cmd[kwargs.pop('cmd')]
            groups.append(cmd.add_mutually_exclusive_group(*args, **kwargs))
        else:
            for cmd in self.sub_cmd.values():
                groups.append(cmd.add_mutually_exclusive_group(*args, **kwargs))
        return groups

    def add_argument(self, *args, **kwargs):
        if 'cmd' in kwargs:
            cmd = self.sub_cmd[kwargs.pop('cmd')]
            cmd.add_argument(*args, **kwargs)
        else:
            for parser in self.sub_cmd.values():
                parser.add_argument(*args, **kwargs)

    def parse(self, *args):
        # Parse the given arguments
        parsed_args = self.parser.parse_args(*args)

        # Transform the sub-command into the corresponding mode
        parsed_args.mode = Mode[parsed_args.cmd.upper()]
        # Delete the sub-command property which has been replaced by the mode
        del parsed_args.cmd

        return parsed_args


class MainLoop(object):
    """
    Collect code that is common to all game playing experiments.
    """

    def __init__(self, env, players, headless=False, video=True):
        """
        Store references to the different players and the environment.
        Initialize the logging facility.
        Initialize miscellaneous objects required for running the main loop of an RL environment.
        :param env: An instance of a PettingZoo environment.
        :param players: A dictionary mapping player names to their corresponding instances.
        :param headless: A boolean flag describing the environment in which the program is executed.
        :param video: A boolean flag enabling the rendering of a video of the environment being executed.
        """

        # Suppress any warning from the environment
        EnvLogger.suppress_output()

        # Define our own logger
        logger.remove()
        logger.add(f"""{env.metadata['name']}.log""",
                   format="""{level}:{module}:{time:YY/MM/DD HH:mm:ss} - {message}""")

        # Instantiate a writer to output a video of the game when testing the agents
        self.video_writer = None
        if video:
            self.video_writer = cv.VideoWriter()

        # Store the configuration files, players and environment
        self.players = players
        self.env = env
        self.env.reset()
        self.headless = headless

        # Declare a history for each player
        self.history = []
        self.losses = defaultdict(list)
        self.rewards = {}
        self.moves = 0

    def __str__(self):
        """
        Describe the players and environment used by the current instance of the MainLoop class.
        :return: A string.
        """
        players = [str(player) for player in self.players.values()]
        env = ['Environment: ', f'\tname: {self.env.metadata["name"]}',
               f'\tAgents: {self.env.possible_agents} (max: {self.env.num_agents})']
        return '\n'.join(env + players)

    def run(self, mode, epochs, max_iter, init_epoch=0, epoch_save_thres=1000):
        """
        Manage all the processes and variables required to execute the main loop of a game playing experiment.
        :param mode: The mode in which the loop is being run. Either train, test, or self-play.
        :param epochs: An integer representing the number of times the same game should be played.
        :param max_iter: An integer representing the maximum number of action agents can take in a particular game.
        :param init_epoch: TODO
        :param epoch_save_thres: An integer representing the number of epochs to wait before making a new checkpoint.
        :return: Nothing.
        """

        # Describe the environment and agents used in the current run
        logger.info(str(self))

        # If the main loop is running in test mode but not in a headless environment
        if mode == Mode.TEST and not self.headless:
            # Create the window in which the game frames will be rendered
            cv.namedWindow('main', cv.WINDOW_NORMAL)

        try:
            # Train both agents
            for epoch in tqdm(range(init_epoch, epochs), total=epochs, desc='Epochs', initial=init_epoch):
                # Declare a dictionary to store the previous state and action for each player
                t_minus_one = {name: {'state': None, 'action': None} for name in self.players.keys()}
                for name in self.env.agent_iter(max_iter=max_iter):
                    # Get the corresponding player
                    player = self.players[name]

                    # Get the new state for the current agent
                    obs_dict, reward, terminated, truncated, info = self.env.last()

                    # Transpose the observations into the torch format
                    obs_dict['observation'] = obs_dict['observation'].transpose(2, 0, 1)

                    # Convert the agent's history to tensors
                    obs_dict['observation'] = torch.from_numpy(obs_dict['observation']).to(dtype=torch.float32,
                                                                                           device=PT_DEVICE)
                    obs_dict['action_mask'] = torch.from_numpy(obs_dict['action_mask']).to(device=PT_DEVICE,
                                                                                           dtype=torch.int)

                    reward = torch.tensor([reward], device=PT_DEVICE, dtype=torch.float32)

                    # Extract the current state
                    curr_state = obs_dict['observation']

                    # Extract the previous state and action
                    prev_state, prev_action = t_minus_one[name]['state'], t_minus_one[name]['action']

                    # If the player has not been terminated
                    done = False
                    if terminated or truncated:
                        done = terminated
                        # This is required to take the agent out of the environment
                        self.env.step(None)
                        # Record the agent's reward for monitoring purposes
                        self.rewards[name] = reward.item()
                    else:
                        # If the player has not been terminated
                        # Decide the next action to perform
                        action = player.get_action(obs_dict, mode)
                        act_itm = action.item()
                        self.env.step(act_itm)

                        # Record the agent's state and action for the next loop
                        t_minus_one[name]['state'] = curr_state
                        t_minus_one[name]['action'] = action

                    if mode != Mode.TEST and prev_state is not None and prev_action is not None:
                        # If in training or self-play, and it is possible to update the agent
                        loss = player.update(prev_state, prev_action, curr_state, reward, done)
                        if loss is not None:
                            self.losses[name].append(loss)

                    if mode == Mode.TEST:
                        # Display and write to video the next game's frame if necessary
                        self.render_frame(epoch)

                # Record the player's history for monitoring purposes
                self.record_history(epoch)

                # Cleanup and reset everything for then next epoch
                self.reset(mode, epoch, epoch_save_thres)

        finally:
            # Close the environment if in TEST mode
            if mode == Mode.TEST:
                self.env.close()
                cv.destroyAllWindows()
                if self.video_writer is not None:
                    self.video_writer.release()
            else:
                self.save_config()

            # Transform the players history into a wide-formatted pandas dataframe
            df = pd.DataFrame(self.history)
            # Transform the Score column into a Categorical for ease of processing in the future
            score_cats = [s.value for s in Score]
            for name in self.players:
                df[f'score_{name}'] = pd.Categorical(df[f'score_{name}'], categories=score_cats, ordered=False)

            return df

    def record_history(self, epoch):
        """
        Insert a new line in the history of the different players, tracking their average loss, reward and score
        for a given epoch.
        :param epoch: An integer representing the current epoch.
        :return: Nothing.
        """

        record = {'epoch': epoch}
        for name, player in self.players.items():
            # Record the reward
            record.update({f'reward_{name}': self.rewards[name]})

            # Record the number of moves played
            record.update({f'moves_{name}': player.nb_moves})
            player.nb_moves = 0

            # Compute the average loss for the epoch
            losses = self.losses[name]
            if len(losses) != 0:
                record.update({f'avg_loss_{name}': sum(losses) / len(losses)})
            else:
                record.update({f'avg_loss_{name}': None})

            # Compute the score for each player. The score is a category indicating whether the player Won or Lost the
            # game, or made an Illegal move
            # If any other player makes an illegal move, then the current player wins by default
            if self.rewards[name] == 1:
                record.update({f'score_{name}': Score.WIN.value})
            elif self.rewards[name] == 0:
                record.update({f'score_{name}': Score.NULL.value})
            else:
                # Check whether the player lost or made an illegal move
                if all([self.rewards[n] == 0 for n in self.players.keys() if n != name]):
                    record.update({f'score_{name}': Score.ILLEGAL.value})
                else:
                    record.update({f'score_{name}': Score.LOSE.value})

        self.history.append(record)
        logger.info(record)
        self.losses = defaultdict(list)

    def reset(self, mode, epoch, epoch_save_thres):
        """
        Resets the game's state between epochs. This is also where new checkpoints are established if necessary.
        :param mode: The mode in which the experiment is executing. Can be either training, testing, or self-play.
        :param epoch: An integer representing the current epoch.
        :param epoch_save_thres: An integer representing the number of epochs to wait between two checkpoints events.
        :return: Nothing.
        """

        # Reset the environment
        self.env.reset()

        # Close any open video file
        if mode == Mode.TEST and self.video_writer is not None and self.video_writer.isOpened():
            self.video_writer.release()

        # Save weights and biases every so often
        if mode != Mode.TEST and epoch % epoch_save_thres == 0:
            # Save the agents' internal states
            self.save_config(epoch)

    def render_frame(self, epoch):
        """
        Depending on the environment in which the program is executing, this method displays the current state of the
        game and write the current frame to a video file. In headless environments, only the video file is written.
        :param epoch: An integer representing the current epoch.
        :return: Nothing.
        """
        if self.video_writer is not None or not self.headless:
            # Render the next frame
            frame = self.env.render()

            # Convert the frame from RGB to BGR since this is what OpenCV expects
            frame = cv.cvtColor(frame, cv.COLOR_RGB2BGR)

            if self.video_writer is not None:
                # Open a new video file and window if necessary
                if not self.video_writer.isOpened():
                    file_name = str(VIDEO_DIR.joinpath(f"""{self.env.metadata['name']}_epoch_{epoch}.avi"""))
                    w, h, c = frame.shape
                    fourcc = cv.VideoWriter.fourcc(*'XVID')
                    is_color = c > 1
                    # It is important to note that h and w are (and should be) inverted
                    # Otherwise, OpenCV produces invalid videos
                    self.video_writer.open(file_name, fourcc, self.env.metadata['render_fps'], (h, w), is_color)

                # Write the frame to the video
                self.video_writer.write(frame)
            if not self.headless:
                cv.imshow('main', frame)
                # Wait key is required for actually drawing the window and its content
                # It can be used to increase/decrease the display's speed
                cv.waitKey(150)

    def save_config(self, epoch=None):
        """
        Establishes a new checkpoint for each agent in turn.
        :param epoch: An integer representing the current epoch.
        :return: Nothing.
        """

        # Save the agents' internal states
        for player in self.players.values():
            player.save_config(game=self.env.metadata['name'], epoch=epoch)
