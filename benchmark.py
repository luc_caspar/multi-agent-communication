#!/usr/bin/env python3
# from datetime import datetime
from argparse import ArgumentParser
import h5py
from pettingzoo.classic import go_v5
from loguru import logger
from agents import KataGoAgent, GoAgent
from settings import CONF_DIR, DATA_DIR
from common import MainLoop
from utils import Mode

# Define a rudimentary CLI to set the different parameters
parser = ArgumentParser()
parser.add_argument('--headless', dest='headless', action='store_true',
                    help='A flag indicating whether the currently playing game should be displayed or not.')
parser.add_argument('--video', dest='video', action='store_true',
                    help='A flag indicating whether a video should be generated from the currently'
                         ' playing game or not.')
parser.add_argument('--nb-games', dest='nb_games', type=int, default=100,
                    help='The number of games to play between KataGo and each version of the GoAgent '
                         'being benchmarked.')
parser.add_argument('--board-size', dest='board_size', type=int, default=9,
                    help='The size of the Go board to use. Standard sizes are: 9, 13, and 19 (full-size).')
parser.add_argument('--komi', dest='komi', type=float, default=7.5,
                    help='The handicap given to white for always starting to move second.')

ex_grp = parser.add_mutually_exclusive_group(required=True)
ex_grp.add_argument('--white', dest='black', action='store_false',
                    help='A flag indicating whether the GoAgent should use the white stones or not.')
ex_grp.add_argument('--black', dest='black', action='store_true',
                    help='A flag indicating whether the GoAgent should use the black stones or not.')

# Parse the command line arguments
args = parser.parse_args()

# Define some parameters for the benchmark
HEADLESS = args.headless
VIDEO = args.video
NB_GAMES = args.nb_games
# TODAY = datetime.today().strftime('%d%m%y_%H%M')

# Instantiate the Go environment
BOARD_SIZE = args.board_size
KOMI = args.komi
env = go_v5.env(board_size=BOARD_SIZE, komi=KOMI, render_mode='rgb_array')

# Instantiate both players
# The players' names also define who plays the first move.
# TODO: It would make sense to test your GoAgent in both position.
KATAGO_NAME = env.possible_agents[int(args.black)]  # White
OURS_NAME = env.possible_agents[1 - int(args.black)]  # Black

observation_shape = env.observation_space(OURS_NAME)['observation'].shape
players = {KATAGO_NAME: KataGoAgent(KATAGO_NAME, BOARD_SIZE, KOMI),
           OURS_NAME: GoAgent(OURS_NAME, obs_shape=observation_shape)}

# Define a data folder specific to this type of benchmark and make sure it exists
data_dir = DATA_DIR.joinpath(f'Go_{BOARD_SIZE}')
if not data_dir.exists():
    data_dir.mkdir(mode=0o775)

if args.black:
    data_file = data_dir.joinpath(f'benchmark_{BOARD_SIZE}_black.h5')
else:
    data_file = data_dir.joinpath(f'benchmark_{BOARD_SIZE}_white.h5')

# Collect the configuration files to be benchmarked
# This store the epoch as key, and the absolute path to the configuration file as the associated value
conf_files = {p.stem.split('_')[-1]: str(p) for p in CONF_DIR.glob('go_v5_self_play_*.pth')}

# Remove any file that was already tested
if data_file.exists():
    with h5py.File(data_file, mode='r') as h5_file:
        for k in h5_file.keys():
            epoch = k.split('_')[-1]
            conf_files.pop(epoch)
logger.info('Benchmarking the following configurations:')
for v in conf_files.values():
    logger.info(f'\t{v}')

# Benchmark all configurations of the GoAgent
for epoch, cfg_path in conf_files.items():
    # Load the new configuration
    players[OURS_NAME].load_config(cfg_path)

    # Reset the environment
    env.reset()

    # Create the main loop
    main_loop = MainLoop(env, players, headless=HEADLESS, video=VIDEO)

    # Run the main loops for the required number of games and gather the games' metrics
    metrics = main_loop.run(Mode.TEST, epochs=NB_GAMES, max_iter=1e9)

    # Save the metrics to disk for later processing
    metrics.to_hdf(data_file, mode='a', key=f'epoch_{epoch}', complevel=9, format='table', index=False)

    # Reset the KataGo agent
    if not players[KATAGO_NAME].reset():
        logger.error('An error occurred while KataGo cleared its board.')
        break

# Close the katago agent
players[KATAGO_NAME].close()
