#!/usr/bin/env python3
from argparse import ArgumentParser
import cv2 as cv
from chess import Board, Move
from agents import Lc0PersonalityAgent
from settings import VIDEO_DIR
from renderers import ChessVideoRenderer, ChessGuiRenderer


parse = ArgumentParser()
ex_grp = parse.add_mutually_exclusive_group()
ex_grp.add_argument('--video', dest='video', action='store_true')
ex_grp.add_argument('--gui', dest='gui', action='store_true')
args = parse.parse_args()


# Define properties associated with the generated video
VIDEO_SIZE = 1000
VIDEO_FPS = 2


if __name__ == "__main__":
    board = Board()

    if args.gui:
        # Instantiate a renderer
        renderer = ChessGuiRenderer(VIDEO_SIZE)
        renderer.start()
    else:
        if args.video:
            # Instantiate a renderer
            fourcc = cv.VideoWriter.fourcc(*'XVID')
            renderer = ChessVideoRenderer(VIDEO_DIR.joinpath('lc0_chess.avi'), VIDEO_SIZE, VIDEO_FPS, fourcc, True)
            renderer.start()
        # Instantiate an agent for the white pieces
        white = Lc0PersonalityAgent((-1, -1, -1), nn_size='s')

    # Instantiate an agent for the Nukabot
    black = Lc0PersonalityAgent((-1, 1, 1), nn_size='s')

    try:
        # Play a game of chess
        cnt = 0
        outcome = board.outcome()
        while outcome is None:
            # Play the next move
            if cnt % 2 == 0:
                if args.gui:
                    move = renderer.q_out.get()
                    renderer.q_out.task_done()
                else:
                    move = white.get_action()
            else:
                move = black.get_action()

            # The last player could not or did not move, simply stop the game
            if move is None:
                break

            # Update both players, the board and the renderer
            if not args.gui:
                white.update(move)
            black.update(move)
            board.push(Move.from_uci(move))
            renderer.q_in.put(move)

            # Increment the move counter
            cnt += 1

            # Check what the outcome is
            outcome = board.outcome()

        # Let the user know about the outcome
        if outcome is not None:
            print(f'Game ended: reason: {outcome.termination}, winner: {outcome.winner}, score: {outcome.result()}')

    finally:
        if args.gui or args.video:
            # Terminate the renderer
            renderer.q_in.put(None)
            if args.gui:
                while not renderer.q_out.empty():
                    renderer.q_out.get()
                    renderer.q_out.task_done()
                renderer.q_out.join()
            renderer.join()
